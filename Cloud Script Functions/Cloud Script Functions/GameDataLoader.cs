﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using Cloud_Script_Functions;

/// <summary>
/// GameDataLoader class
/// It will be in charge of loading data and asset bundles.
/// needs to be destroyed after everything is loaded. 
/// </summary>
namespace StratosphereGames.Base
{
    public class GameDataLoader
    {
        /*
        public class DownloadContext
        {
            public BackendErrorCode errorCode = BackendErrorCode.S_OK;
        }

        // Use this for initialization
        void Start()
        {
            //// Sample code
            //StartCoroutine(LoadAll_Sample());
        }

        public IEnumerator InitializeAssetBundles(DownloadContext context)
        {
            bool bProduction = false;

#if _PRODUCTION_
	        bProduction = true;
#endif
            //string srcUrl = "http://nemesis-thegame.com/warzone/AssetBundleInternal" + stage + "/" + phase + "/";
            string srcUrl = "http://nemesis-thegame.com/"
                + StratoUtility.GetRemoteAssetBundlePath(bProduction, AssetBundles.Utility.GetPlatformName()) + "/";
            string srcCloudFrontUrl = "http://d380royxfrm22x.cloudfront.net/"
                + StratoUtility.GetRemoteAssetBundlePath(bProduction, AssetBundles.Utility.GetPlatformName()) + "/";

#if !_PRODUCTION_
            Debug.Log("[SLog] Asset bundle : " + srcUrl);
#endif
            int dataVersion = -1;

            {
                // Get DataVersion File
                string dataVersionUrl = srcUrl + "DataVersion.json";

                WWW dVersion = new WWW(dataVersionUrl);
                yield return dVersion;

                if (!string.IsNullOrEmpty(dVersion.error))
                {
                    Debug.LogError(dVersion.error);
                    context.errorCode = BackendErrorCode.DownloadFailed;
                    yield break;
                }
                else
                {
                    Debug.LogError("Missing DataVersion class. Please implement.");
                    throw new System.NotImplementedException();
                    //var d = JsonWrapper.DeserializeObject<DataVersion>(dVersion.text);
                    //if (d != null)
                    //{
                    //    dataVersion = d.version;
                    //}
                }
            }

            if (dataVersion != -1)
            {
                srcCloudFrontUrl += dataVersion + "/";
            }

            AssetBundleManager.SetSourceAssetBundleURL(srcCloudFrontUrl);
            AssetBundleManager.logMode = AssetBundleManager.LogMode.All;

            var requestInit = AssetBundleManager.Initialize();
            if (requestInit != null)
                yield return StartCoroutine(requestInit);

            yield return null;
        }

        // Sample code
        //IEnumerator LoadAll_Sample()
        //{
        //       var container = new List<cardArchetypes_Military>();
        //       var containerSomethingElse = new List<cardArchetypes_Military>();

        //       List<IEnumerator> arrayTasks = new List<IEnumerator>();

        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", containerSomethingElse));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));
        //       arrayTasks.Add(LoadJSONFile<cardArchetypes_Military>("cardArchetypes_Military", container));

        //       foreach (var task in arrayTasks)
        //    {
        //        yield return task;
        //    }

        //    Debug.Log("Done");
        //}

        public static void CopyDictionaryToObject<T>(T targetObject, IDictionary<string, object> dict)
        {
            PropertyInfo[] properties = targetObject.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (!dict.Any(x => x.Key.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase)))
                {
                    continue;
                }

                KeyValuePair<string, object> kv = dict.First(x => x.Key.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase));

                Type tPropertyType = targetObject.GetType().GetProperty(property.Name).PropertyType;
                Type newT = Nullable.GetUnderlyingType(tPropertyType) ?? tPropertyType;

                try
                {
                    object value = kv.Value;
                    if (tPropertyType.IsGenericType)
                    {
                        var ser = JsonWrapper.SerializeObject(kv.Value);
                        var val = SimpleJson.SimpleJson.DeserializeObject(ser, tPropertyType);

                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, val, null);
                    }
                    else if (tPropertyType.IsClass)
                    {
                        var ser = JsonWrapper.SerializeObject(kv.Value);
                        var val = SimpleJson.SimpleJson.DeserializeObject(ser, tPropertyType);

                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, val, null);
                    }
                    else if (tPropertyType.IsEnum)
                    {
                        object newA = Enum.Parse(tPropertyType, value.ToString());
                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, newA, null);
                    }
                    else if (value != null && tPropertyType == typeof(SecuredInt))
                    {
                        int iValue = (int)Convert.ChangeType(value, typeof(int));
                        SecuredInt sValue = iValue;
                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, sValue, null);
                    }
                    else
                    {
                        object newA = value == null ? null : Convert.ChangeType(value, newT);
                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, newA, null);
                    }
                }
                catch (Exception e)
                {
                    Debug.LogException(e);
                }
            }
        }
         */
        public static T DictionaryToObject<T>(IDictionary<string, object> dict) where T : new()
        {
            var targetObject = new T();
            PropertyInfo[] properties = targetObject.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (!dict.Any(x => x.Key.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase)))
                {
                    continue;
                }

                KeyValuePair<string, object> kv = dict.First(x => x.Key.Equals(property.Name, StringComparison.InvariantCultureIgnoreCase));

                Type tPropertyType = targetObject.GetType().GetProperty(property.Name).PropertyType;
                Type newT = Nullable.GetUnderlyingType(tPropertyType) ?? tPropertyType;

                try
                {
                    object value = kv.Value;
                    if (tPropertyType.IsGenericType)
                    {
                        var ser = JsonWrapper.SerializeObject(kv.Value);
                        var val = SimpleJson.SimpleJson.DeserializeObject(ser, tPropertyType);

                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, val, null);
                    }
                    else if (tPropertyType.IsEnum)
                    {
                        object newA = Enum.Parse(tPropertyType, value.ToString());
                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, newA, null);
                    }
                    else if (value != null && tPropertyType == typeof(SecuredInt))
                    {
                        int iValue = (int)Convert.ChangeType(value, typeof(int));
                        SecuredInt sValue = iValue;
                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, sValue, null);
                    }
                    else
                    {
                        if(value == null || string.IsNullOrEmpty(value.ToString())) //Robin workaround for empty fields
                        {
                            continue;
                        }

                        object newA = value == null ? null : Convert.ChangeType(value, newT);
                        targetObject.GetType().GetProperty(property.Name).SetValue(targetObject, newA, null);
                    }
                }
                catch (Exception e)
                {
                    LoggingHandler.Log(e.Message, LogType.Error);
                }
            }
            return targetObject;
        }
       
        public static Dictionary<string, object> ObjectToDictionary(object objectToConvert)
        {
            Dictionary<string, object> newDictionary = new Dictionary<string, object>();
            PropertyInfo[] properties = objectToConvert.GetType().GetProperties();

            foreach (PropertyInfo prop in properties)
            {
                var v = prop.GetValue(objectToConvert, null);
                //if (v != null && v.GetType().IsClass)
                //if (v is IList)
                //{
                //    var listObj = ObjectToList(v);
                //    newDictionary.Add(prop.Name, listObj);
                //}
                if (v != null && !(v is System.String) && !(v is IDictionary) && !(v is IList) && v.GetType().IsClass)
                {
                    newDictionary.Add(prop.Name, ObjectToDictionary(v));
                }
                else
                {
                    newDictionary.Add(prop.Name, v);
                }
            }

            return newDictionary;
        }
        /*
        public static bool CopyObject(object objTo, object objFrom)
        {
            if (objTo == null || objFrom == null)
            {
                return false;
            }
            foreach (PropertyInfo propA in objFrom.GetType().GetProperties())
            {
                PropertyInfo propB = objTo.GetType().GetProperty(propA.Name);
                if (propB != null)
                    propB.SetValue(objTo, propA.GetValue(objFrom, null), null);
            }
            return true;
        }

        public static bool CopyObject(object objTo, IDictionary<string, object> dictFrom)
        {
            if (objTo == null || dictFrom == null)
            {
                return false;
            }
            foreach (PropertyInfo propA in objTo.GetType().GetProperties())
            {
                if (dictFrom.ContainsKey(propA.Name))
                {
                    propA.SetValue(objTo, dictFrom[propA.Name], null);
                }
            }
            return true;
        }

        public static void CopyDictionary(IDictionary<string, object> dictTo, IDictionary<string, object> dictFrom)
        {
            dictTo.Clear();
            foreach (var kv in dictFrom)
            {
                dictTo.Add(kv.Key, kv.Value);
            }
        }

        public static List<object> ObjectToList(object objectToConvert)
        {
            List<object> newList = new List<object>();
            IList listCollection = (IList)objectToConvert;
            foreach (object o in listCollection)
            {
                if (o.GetType().IsClass)
                //if (o is Dictionary<string, object>)
                {
                    var dict = ObjectToDictionary(o);
                    newList.Add(dict);
                }
                else
                {
                    newList.Add(o);
                }
            }

            return newList;
        }

        public IEnumerator LoadJSONFile<T>(
            string fileName,
            List<T> container
            ) where T : AttributeDataEntity, new()
        {
            container.Clear();
            StringBuilder sBuilder = new StringBuilder();

            sBuilder.Append(@"file://")
                .Append(Application.dataPath)
                .Append(Path.DirectorySeparatorChar)
                .Append("DataTable")
                .Append(Path.DirectorySeparatorChar)
                .Append(fileName)
                .Append(".json");
            WWW www = new WWW(sBuilder.ToString());
            yield return www;
            if (www.isDone)
            {
                string str = www.text;
                if (www.error == null)
                {
                    Debug.Log(str);
                    var listData = Json.Deserialize(str) as IList;
                    for (int i = 0; i < listData.Count; ++i)
                    {
                        var item = listData[i] as Dictionary<string, object>;

                        T tmp = new T();
                        tmp.Deserialize(tmp, item);
                        //var tmp = GameDataLoader.DictionaryToObject<T>(item);

                        container.Add(tmp);
                    }
                }
                else
                {
                    Debug.LogWarning(www.error);
                }
            }
        }

        public IEnumerator LoadAssetBundleJSONFile<T>(string fileName, List<T> container) where T : new()
        {
            string assetName = "datatable";

            container.Clear();

            AssetBundleLoadAssetOperation request =
                AssetBundleManager.LoadAssetAsync(assetName, fileName, typeof(TextAsset));

            if (request == null)
            {
                Debug.LogError("Failed AssetBundleLoadAssetOperation on "
                    + assetName +
                    " from the AssetBundle "
                    + fileName + ".");
                yield break;
            }

            yield return request;

            TextAsset asset = request.GetAsset<TextAsset>();
            if (asset != null)
            {
                string str = asset.text;
                //Debug.Log(str);

                IList listData = Json.Deserialize(str) as IList;
                for (int i = 0; i < listData.Count; ++i)
                {
                    var item = listData[i] as Dictionary<string, object>;
                    var tmp = GameDataLoader.DictionaryToObject<T>(item);
                    container.Add(tmp);
                }
            }

            yield return null;
        }

        public IEnumerator LoadAssetBundleJSONFileToAttributeData<T>(string fileName, List<T> container)
            where T : AttributeDataEntity, new()
        {
            string assetName = "datatable";

            container.Clear();

            AssetBundleLoadAssetOperation request =
                AssetBundleManager.LoadAssetAsync(assetName, fileName, typeof(TextAsset));

            if (request == null)
            {
                Debug.LogError("Failed AssetBundleLoadAssetOperation on "
                    + assetName +
                    " from the AssetBundle "
                    + fileName + ".");
                yield break;
            }

            yield return request;

            TextAsset asset = request.GetAsset<TextAsset>();
            if (asset != null)
            {
                string str = asset.text;
                //Debug.Log(str);

                IList listData = Json.Deserialize(str) as IList;
                for (int i = 0; i < listData.Count; ++i)
                {
                    var item = listData[i] as Dictionary<string, object>;
                    T tmp = GameDataLoader.DictionaryToObject<T>(item);
                    container.Add(tmp);
                }
            }

            yield return null;
        }
        */
    }
}
