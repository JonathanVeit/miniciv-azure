using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PlayFab.ServerModels;
using PlayFab;
using MiniCiv;
using System.Linq;
using StratosphereGames.Helpers;
using PlayFab.CloudScriptModels;
using System.Diagnostics;

namespace Cloud_Script_Functions
{
    public static class CloudScriptFunctions
    {
        #region Static variables
        private static Dictionary<string, string> resyncRequest = new Dictionary<string, string>() { { "DoResync", "true" } };

        private static Dictionary<string, PlayerMatch> currentMatches = new Dictionary<string, PlayerMatch>();
        #endregion

        #region Player Initialization
        [FunctionName("InitializePlayer")]
        public static async Task InitializePlayer(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetPlaystreamContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.PlayerProfile.PlayerId;
            var fLogger = new FunctionLogger("InitializePlayer", playerID, true);

            // get initialization data 
            var rawData = await PFCommunication.GetTitleData(new List<string>() { "PlayerInitData" }, PlayFabDataType.Internal, fLogger);

            PlayerInitializationData playerInitData = default;
            try { playerInitData = JsonConvert.DeserializeObject<PlayerInitializationData>(rawData["PlayerInitData"]); }
            catch (Exception e) { fLogger.LogException("Cannot deserialize Player Initialization data: " + e.Message); }

            // Add characters 
            var allCharacters = new Dictionary<string, string>();

            foreach (var curCharacter in playerInitData.characters)
            {
                var ID = await PFCommunication.GrantCharacterToPlayer(playerID, curCharacter.characterName, curCharacter.characterType, fLogger);

                allCharacters.Add(curCharacter.characterName, ID);
            }

            // building character ID 
            string bcID = allCharacters[playerInitData.buildingsCharacter];

            // load each unit and store them with level 1 in dic
            var defaultUnitLevels = new Dictionary<string, int>();
            GameDataManager.GetAllTableData(out UnitData[] allUnitsData);
            foreach (var curUnit in allUnitsData)
            {
                defaultUnitLevels.Add(curUnit.UniqueId, 1);
            }

            List<Task> t_FinishInit = new List<Task>();

            // set characters, unit levels, last enemy Id and army setups 
            t_FinishInit.Add(PFCommunication.UpdateUserData(playerID,
                new Dictionary<string, string>()
                {
                    { "Characters", JsonConvert.SerializeObject(allCharacters) },
                    { "UnitLevels", JsonConvert.SerializeObject(defaultUnitLevels) },
                    { "UpgradingUnits", JsonConvert.SerializeObject(new Dictionary<string, string> ()) },
                    { "LastEnemyIDs", JsonConvert.SerializeObject(new Queue<string> ()) },
                    { "ArmySetups", JsonConvert.SerializeObject(new List<ArmySetup>() { new ArmySetup(true) }) },
                    { "OfflineMatches", JsonConvert.SerializeObject(new List <PlayerMatch>()) }
                },
                PlayFabDataType.Internal, fLogger));

            // add default units
            t_FinishInit.Add (PFCommunication.GrantItemsToCharacter(playerID, allCharacters["UnitsOwned"], "Units", playerInitData.defaultUnits, fLogger));

            // add buildings to character
            var t_GrantItemsToCharacter = PFCommunication.GrantItemsToCharacter(playerID, bcID, "Buildings", playerInitData.buildingItemsToAdd, fLogger);

            // initialize buildings
            var dataDic = new Dictionary<string, string>();
            foreach (var curData in playerInitData.buildingItemData)
            {
                dataDic.Add(curData.key, curData.useDatetime ? JsonConvert.SerializeObject(DateTime.Now.ToUniversalTime()) : curData.value);
            }

            var allBuildings = await t_GrantItemsToCharacter;
            foreach (var curResult in allBuildings.ItemGrantResults)
            {
                t_FinishInit.Add(PFCommunication.UpdateItemCustomData(playerID, bcID, curResult.ItemInstanceId, dataDic, fLogger));
            }

            // wait for all tasks to be completed
            await Task.WhenAll(t_FinishInit);

            // set player statistics -> at the end so the FinishedPlayerInitialization request can work 
            var statisticsUpdate = new List<StatisticUpdate>();
            foreach (var curStatistic in playerInitData.statistics)
            {
                var newSU = new StatisticUpdate();
                newSU.StatisticName = curStatistic.name;
                newSU.Value = curStatistic.value;
                statisticsUpdate.Add(newSU);
            }

            await PFCommunication.UpdatePlayerStatistics(playerID, statisticsUpdate, fLogger);

            fLogger.LogAll();
        }

        [FunctionName("FinishedPlayerInitialization")]
        public static async Task<bool> FinishedPlayerInitialization(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("InitializePlayer", playerID, true);

            // check user data
            var result = await PFCommunication.GetPlayerStatistics(playerID, new List<string> { "Battle Points" }, fLogger);

            return result.Count > 0 ? true : false;
        }
        #endregion

        #region Get Player Information
        [FunctionName("GetPlayerCurrencies")]
        public static async Task<dynamic> GetPlayerCurrencies(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player 
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("GetPlayerCurrencies", playerID, true);

            var result = await PFCommunication.GetPlayerCurrencies(playerID, fLogger);

            fLogger.LogEnd();
            return result;
        }

        [FunctionName("GetPlayerBuildings")]
        public static async Task<dynamic> GetPlayerBuildings(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player 
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("GetPlayerBuildings", playerID, true);

            // required data
            string bcID = await PFCommunication.GetCharacterID(playerID, "Buildings", fLogger);

            var buildings = await PFCommunication.GetCharacterInventoryByID(playerID, bcID, fLogger);

            // update item data tasks
            List<Task> t_UpdateItemData = new List<Task>();
         
            // check if upgrades where finished
            foreach (var curBuilding in buildings)
            {
                if (StringIsNotNullOrEmpty(curBuilding.CustomData["UpgradeStartedAt"]))
                    continue;

                var upgradeStartedAt = JsonConvert.DeserializeObject<DateTime>(curBuilding.CustomData["UpgradeStartedAt"]);
                int curLevel = int.Parse(curBuilding.CustomData["Level"]);

                if (TimeDifferenceMS(upgradeStartedAt, DateTime.Now) > -10000)
                {
                    // change data for instance locally
                    curBuilding.CustomData["UpgradeStartedAt"] = "null";
                    curBuilding.CustomData["Level"] = (curLevel + 1).ToString();

                    // update data on playfab
                    t_UpdateItemData.Add(PFCommunication.UpdateItemCustomData(playerID, bcID, curBuilding.ItemInstanceId,
                        new Dictionary<string, string>()
                        {
                            {"Level", (curLevel + 1).ToString()},
                            {"UpgradeStartedAt", "null" },
                        }, fLogger));

                    // log 
                    fLogger.AddMessage("Finished upgrade of building \"" + curBuilding.ItemId + "\" to level: " + (curLevel + 1).ToString());
                }
            }
            
            // serialize custom data for each building into dic
            var buildingsAsDic = new Dictionary<string, string>();
            foreach (var curBuilding in buildings)
            {
                buildingsAsDic.Add(curBuilding.ItemId, JsonConvert.SerializeObject(curBuilding.CustomData));
            }

            await Task.WhenAll(t_UpdateItemData);

            fLogger.LogAll();
            return buildingsAsDic;
        }

        [FunctionName("GetPlayerUnits")]
        public static async Task<dynamic> GetPlayerUnits(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player 
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("GetPlayerUnits", playerID, true);

            // required data
            var allCharacerIDs = await PFCommunication.GetCharacterIDs(playerID, fLogger);
            var utcID = allCharacerIDs["UnitsTraining"];    // units training character ID
            var uocID = allCharacerIDs["UnitsOwned"];       // units owned character ID
            var uwcID = allCharacerIDs["UnityWounded"];     // units wounded character ID

            var unitsTraining = await PFCommunication.GetCharacterInventoryByID(playerID, utcID, fLogger);
            var unitsOwned = await PFCommunication.GetCharacterInventoryByID(playerID, uocID, fLogger);
            var unitsWounded = await PFCommunication.GetCharacterInventoryByID(playerID, uwcID, fLogger);

            // moving units to owned
            List<Task> t_CheckUnitsTraining = new List<Task>();

            // move training units to owned
            for (int i = 0; i < unitsTraining.Count; i++)
            {
                var startDate = JsonConvert.DeserializeObject<DateTime>(unitsTraining[i].CustomData["StartedTrainingAt"]);

                // get duration
                GameDataManager.GetDataForTableElement<UnitData>(unitsTraining[i].ItemId, out UnitData uData);
                int dur = 100; //todo duration in UnitData?

                // check if trainign is over
                if (TimeDifferenceMS(startDate, DateTime.Now) >= dur)
                {
                    // move item in playfab
                    t_CheckUnitsTraining.Add(PFCommunication.MoveItemToCharacter(playerID, utcID, uocID, unitsTraining[i].ItemInstanceId, fLogger));

                    // move item in local list 
                    unitsOwned.Add(unitsTraining[i]);
                    unitsTraining.RemoveAt(i);

                    // log 
                    fLogger.AddMessage("Unit \"" + unitsTraining[i].ItemId + "\" finished training and was moved to UnitsOwned" +
                                "\nTraining started at: " + startDate.ToString() +
                                "\nDuration was: " + "1 s");
                }
            }

            // move wounded units to owned
            for (int i = 0; i < unitsWounded.Count; i++)
            {
                var startDate = JsonConvert.DeserializeObject<DateTime>(unitsWounded[i].CustomData["StartedHealingAt"]);

                // check if trainign is over
                if (TimeDifferenceMS(startDate, DateTime.Now) > 100) // todo: get this duration from titel data
                {
                    // move item in playfab
                    t_CheckUnitsTraining.Add(PFCommunication.MoveItemToCharacter(playerID, uwcID, uocID, unitsWounded[i].ItemInstanceId, fLogger));

                    // move item in local list 
                    unitsOwned.Add(unitsWounded[i]);
                    unitsWounded.RemoveAt(i);

                    // log 
                    fLogger.AddMessage("Unit \"" + unitsTraining[i].ItemId + "\" finished healing and was moved to UnitsOwned" +
                                "\nHealing started at: " + startDate.ToString() +
                                "\nDuration was: " + "1" + " s"); // todo: healing duration?
                }
            }

            // store data in dictionary
            var unitsAsDic = new Dictionary<string, string>();

            // add units training
            {
                string[] unityIDs = new string[unitsTraining.Count];
                for (int i = 0; i < unitsTraining.Count; i++)
                {
                    string[] IDandAmount = new string[3];
                    IDandAmount[0] = unitsTraining[i].ItemId;
                    IDandAmount[1] = unitsTraining[i].RemainingUses.ToString();
                    IDandAmount[2] = unitsTraining[i].CustomData["StartedTrainingAt"];
                    unityIDs[i] = JsonConvert.SerializeObject(IDandAmount);
                }

                unitsAsDic.Add("UnitsTraining", JsonConvert.SerializeObject(unityIDs));
            }

            // add units owned
            {
                string[] unityIDs = new string[unitsOwned.Count];
                for (int i = 0; i < unitsOwned.Count; i++)
                {
                    string[] IDandAmount = new string[2];
                    IDandAmount[0] = unitsOwned[i].ItemId;
                    IDandAmount[1] = unitsOwned[i].RemainingUses.ToString();

                    unityIDs[i] = JsonConvert.SerializeObject(IDandAmount);
                }

                unitsAsDic.Add("UnitsOwned", JsonConvert.SerializeObject(unityIDs));
            }

            // add units wounded
            {
                string[] unityIDs = new string[unitsWounded.Count];
                for (int i = 0; i < unitsWounded.Count; i++)
                {
                    string[] IDandAmount = new string[3];
                    IDandAmount[0] = unitsWounded[i].ItemId;
                    IDandAmount[1] = unitsWounded[i].RemainingUses.ToString();
                    IDandAmount[3] = unitsWounded[i].CustomData["WoundedAt"];

                    unityIDs[i] = JsonConvert.SerializeObject(IDandAmount);
                }

                unitsAsDic.Add("UnityWounded", JsonConvert.SerializeObject(unityIDs));
            }

            await Task.WhenAll(t_CheckUnitsTraining);

            fLogger.LogEnd();
            return unitsAsDic;
        }

        [FunctionName("GetPlayerUnitLevels")]
        public static async Task<dynamic> GetPlayerUnitLevels(
           [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
           ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("GetPlayerUnitLevels", playerID, true);

            // get character IDs
            var userData = await PFCommunication.GetUserData(playerID, new List<string>() { "UnitLevels" }, PlayFabDataType.Internal, fLogger);

            var unitLevels = JsonConvert.DeserializeObject<Dictionary<string, int>>(userData["UnitLevels"].Value);

            fLogger.LogEnd();
            return unitLevels;
        }

        [FunctionName("GetPlayerDisplayName")]
        public static async Task<dynamic> GetPlayerDisplayName(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
             ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("GetPlayerDisplayName", playerID, true);

            var accountInfo = await PFCommunication.GetUserAccountInfo(playerID, fLogger);

            fLogger.LogEnd();
            return accountInfo.UserInfo.TitleInfo.DisplayName;
        }

        [FunctionName("GetPlayerArmySetups")]
        public static async Task<dynamic> GetPlayerArmySetups(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("GetPlayerArmySetups", playerID, true);

            // get army setups
            var userData = await PFCommunication.GetUserData(playerID, new List<string>() { "ArmySetups" }, PlayFabDataType.Internal, fLogger);

            var setups = JsonConvert.DeserializeObject<List<ArmySetup>>(userData["ArmySetups"].Value);

            fLogger.LogEnd();
            return setups;
        }

        [FunctionName("GetOfflineMatches")]
        public static async Task<dynamic> GetOfflineMatches(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("GetOfflineMatch", playerID, true);

            // get custom data
            var allData = await PFCommunication.GetUserData(playerID, new List<string>() { "OfflineMatches" }, PlayFabDataType.Internal, fLogger);

            // search match 
            var offlineMatchString = allData["OfflineMatches"].Value.ToString(); ;

            if (StringIsNotNullOrEmpty(offlineMatchString))
            {
                fLogger.LogEnd();

                // had no offline match
                return null;
            }
            else
            {
                // reset value
                await PFCommunication.UpdateUserData(playerID, new Dictionary<string, string>() { { "OfflineMatches", "[]"} }, PlayFabDataType.Internal, fLogger);

                fLogger.LogEnd();

                // had an offline match
                return offlineMatchString;
            }
        }
        #endregion

        #region Set Player Information
        [FunctionName("SetPlayerArmySetups")]
        public static async Task<dynamic> SetPlayerArmySetups(
        [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("SetPlayerArmySetups", playerID, true);
            if (!ArgsAreValid(context.FunctionArgument, "ArmySetups"))
            {
                fLogger.LogError("Arguments are not valid");
                return resyncRequest;
            }
            dynamic args = context.FunctionArgument;

            // required data
            string serializedArmySetups = args["ArmySetups"];
            var armySetups = JsonConvert.DeserializeObject<List<ArmySetup>>(serializedArmySetups);

            // get all units owned 
            var ownedUnits = await PFCommunication.GetCharacterInventoryByName(playerID, "UnitsOwned", fLogger);

            // combine owned units in dictionary by id
            var ownedUnitsCombined = new Dictionary<string, int>();
            foreach (var curUnit in ownedUnits)
            {
                ownedUnitsCombined.Add(curUnit.ItemId, (int)curUnit.RemainingUses);
            }

            // check if setup is valid
            int curSetupIndex = 0;
            foreach (ArmySetup curSetup in armySetups)
            {
                // combine units in dictionary by id
                var setupUnitsCombined = new Dictionary<string, int>();
                foreach (var curUnit in curSetup.placedUnits)
                {
                    if (!setupUnitsCombined.ContainsKey(curUnit.uId))
                        setupUnitsCombined.Add(curUnit.uId, 0);

                    setupUnitsCombined[curUnit.uId]++;
                }

                // check if setup is valid
                foreach (string curUnitID in setupUnitsCombined.Keys)
                {
                    // player does not own this unit or does not own enough of it
                    if (!ownedUnitsCombined.ContainsKey(curUnitID))
                    {
                        fLogger.LogError("Army setup " + curSetupIndex + " is not valid. Player does not own unit \"" + curUnitID + "\"");
                        return resyncRequest;
                    }
                    else if (setupUnitsCombined[curUnitID] > ownedUnitsCombined[curUnitID])
                    {
                        fLogger.LogError("Army setup " + curSetupIndex + " is not valid. Player only has " + ownedUnitsCombined[curUnitID].ToString() + " units with ID \"" + curUnitID + "\" " +
                                         "but the setup contains " + setupUnitsCombined[curUnitID].ToString());
                        return resyncRequest;
                    }
                }
                                  
                fLogger.AddMessage("Setup " + curSetupIndex + " is valid");
                curSetupIndex++;
            }

            await PFCommunication.UpdateUserData(playerID, new Dictionary<string, string>() { { "ArmySetups", args["ArmySetups"].ToString() } }, PlayFabDataType.Internal, fLogger);

            fLogger.LogAll();
            return new Dictionary<string, string>() { { "DoResync", "false" } }; 
        }

        [FunctionName("PlayerLevelUp")]
        public static async Task PlayerLevelUp(
        [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetPlaystreamContext(req);
            dynamic args = context.FunctionArgument;
            PlayFab.Json.JsonObject eventArgs = JsonConvert.DeserializeObject <PlayFab.Json.JsonObject >(context.PlayStreamEventEnvelope.EventData);
            string playerID = context.PlayerProfile.PlayerId;
            var fLogger = new FunctionLogger("PlayerLevelUp", playerID, true);
            var c = new PlayStreamEventEnvelopeModel();
            
            // required data     
            string statisticName = eventArgs["StatisticName"].ToString();
            if (statisticName != "XP") return;
            string statisticValueAsString = eventArgs["StatisticValue"].ToString();
            int statisticValue = int.Parse(statisticValueAsString);
            var t_GetLevelAndXP = PFCommunication.GetPlayerStatistics(playerID, new List<string> { "Level", "XP"}, fLogger);

            // get xp for level up
            GameDataManager.GetDataForTableElement("XPForLevelUp", out GlobalVariables valueRaw);

            // convert to list
            var XPForLevelUp = DeserializationHelper.ParseElementToListOfPrimitives<int>(valueRaw.Value, '\n');
            
            // wait for level
            var result = await t_GetLevelAndXP;
            int level = result[0].Value;
            int xp = result[1].Value;

            // get current Level 
            var requiredXP = XPForLevelUp[level - 1];

            // increase level if possible
            if (xp >= requiredXP)
                await PFCommunication.UpdatePlayerStatistics(playerID, new List<StatisticUpdate>() 
                { 
                    new StatisticUpdate 
                    { 
                        StatisticName = "Level",
                        Value = 1,
                    } 
                }, fLogger);

            fLogger.LogAll();
        }
        #endregion

        #region Interaction with Buildings
        [FunctionName("CollectBuildingResources")]
        public static async Task<dynamic> CollectBuildingResources(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);
           
            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            var fLogger = new FunctionLogger("CollectBuildingResources", playerID, true);
            if (!ArgsAreValid(context.FunctionArgument, "BuildingID", "Time", "ExpectedAmount"))
            {
                fLogger.LogError("Arguments are not valid");
                return resyncRequest;
            }
            var args = context.FunctionArgument;
           
            // required data
            string buildingID = args["BuildingID"];                                             // ID of the building
            string dateAsString = args["Time"];                                                 // date when player collected raw    
            DateTime playerColDate = default;                                                   // date when player collected   
            try { playerColDate = JsonConvert.DeserializeObject<DateTime>(dateAsString).ToUniversalTime(); }
            catch (Exception e) { fLogger.LogError("Cannot deserialize player submitted time: " + e.Message); return resyncRequest; }
            if (!TimeIsValid(playerColDate, 50000, out double difference))
            {
                fLogger.LogError("Player submitted time is not valid. Difference to internal date:" + difference + " ms.\n" +
                                 "Player time: " + dateAsString + "\n" +
                                 "Internal time:" + JsonConvert.SerializeObject(DateTime.UtcNow));
                return resyncRequest;
            }                   // check if time is valid
            else
            {
                fLogger.AddMessage("Player submitted time is valid",
                                   "Player time  : " + dateAsString + "\n" +
                                   "Internal time: " + JsonConvert.SerializeObject(DateTime.UtcNow));
            }

            int playerExpectedAmount = default;                                                 // amount the playe expects to collect    
            try { playerExpectedAmount = int.Parse(args["ExpectedAmount"].ToString()); }
            catch (Exception e) { fLogger.LogError("Cannot parse player expected amount: " + e.Message); return resyncRequest; }
            string buildingItemInstanceID = "";
            string lastColDateStr = "";
            int buildingLevel = -1;
            string bcID = await PFCommunication.GetCharacterID(playerID, "Buildings", fLogger); // character ID of the buildings character

            // get buildings as inventory
            var buildings = await PFCommunication.GetCharacterInventoryByID(playerID, bcID, fLogger);

            // get last date of collecting           
            foreach (var curBuilding in buildings)
            {
                if (curBuilding.ItemId == buildingID)
                {
                    buildingItemInstanceID = curBuilding.ItemInstanceId;
                    lastColDateStr = curBuilding.CustomData["LastCollectedAt"];
                    buildingLevel = int.Parse(curBuilding.CustomData["Level"]);
                    break;
                }
            }

            // no building found?
            if (buildingLevel == -1)
            {
                fLogger.LogError("Cannot find building \"" + buildingID + "\"");
                return resyncRequest;
            }

            DateTime lastColDate = JsonConvert.DeserializeObject<DateTime>(lastColDateStr);

            // calculate difference in ms 
            var differenceMS = TimeDifferenceMS(lastColDate, playerColDate);

            // calculate currency amount
            var producedResources = CalculateProducedResources(playerID, buildingID, buildingLevel, (int)differenceMS / 1000);

            // building does not produce resource?
            if (producedResources == null)
            {
                fLogger.LogError("Player requestes collection for building \"" + buildingID + "\" which does not produce resources");
                return resyncRequest;
            }

            string resourcesToGrant = producedResources.Keys.ToArray()[0];
            int calculatedAmount = producedResources.Values.ToArray()[0];

            // check if amount is valid 
            if (playerExpectedAmount > calculatedAmount + 20) // differences of up to +20 are allowed 
            {
                fLogger.LogError("Player submitted amount is " + playerExpectedAmount.ToString() + " but the calculated amount is " + calculatedAmount.ToString());
                return resyncRequest;
            }

            // log amounts
            fLogger.AddMessage("Player expected amount is valid. ",
                               "Expected  : " + playerExpectedAmount + "\n" +
                               "Calculated: " + calculatedAmount);

            // grand currency
            Task[] t_finishCollection = new Task[2];
            t_finishCollection[0] = PFCommunication.AddCurrencyToPlayer(playerID, resourcesToGrant, playerExpectedAmount, fLogger);

            // reset last date of collecting to current
            t_finishCollection[1] = PFCommunication.UpdateItemCustomData(playerID, bcID, buildingItemInstanceID, new Dictionary<string, string>() { { "LastCollectedAt", dateAsString } }, fLogger);

            await Task.WhenAll(t_finishCollection);

            fLogger.LogAll();
            return new Dictionary<string, string>() { { "DoResync", "false" } };
        }

        [FunctionName("StartUpgradeBuilding")]
        public static async Task<dynamic> StartUpgradeBuilding(
        [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            FunctionLogger fLogger = new FunctionLogger("StartUpgradeBuilding", playerID, true);
            if (!ArgsAreValid(context.FunctionArgument, "BuildingID", "StartedUpgradeAt", "Time"))
            {
                fLogger.LogError("Arguments are not valid");
                return resyncRequest;
            }
            var args = context.FunctionArgument;

            // required data
            string buildingID = args["BuildingID"];                                             // ID of the building
            DateTime playerUpgradeDate = default;                                               // date when player started
            try { playerUpgradeDate = JsonConvert.DeserializeObject<DateTime>(((string)args["Time"])).ToUniversalTime(); }
            catch (Exception e) { fLogger.LogError("Cannot deserialize player submitted time: " + e.Message); return resyncRequest; }
            string buildingItemInstanceID = "";
            int buildingLevel = -1;
            int mainBuildingLevel = -1;
            string bcID = await PFCommunication.GetCharacterID(playerID, "Buildings", fLogger); // ID of the buildings character

            // get buildings as inventory
            var buildings = await PFCommunication.GetCharacterInventoryByID(playerID, bcID, fLogger);

            // get instanceID and level of building                                                         
            foreach (var curBuilding in buildings)
            {
                if (curBuilding.ItemId == buildingID)
                {
                    buildingItemInstanceID = curBuilding.ItemInstanceId;
                    buildingLevel = int.Parse(curBuilding.CustomData["Level"]);
                }
                if (curBuilding.ItemId == "building_09")
                    mainBuildingLevel = int.Parse(curBuilding.CustomData["Level"]);
            }

            // no building found?
            if (buildingLevel == -1 || mainBuildingLevel == -1)
            {
                fLogger.LogError("Cannot find building \"" + buildingID + "\"");
                return resyncRequest;
            }

            // get building config
            GameDataManager.GetDataForTableElement<BuildingData>(buildingID, out BuildingData bData);
            var bConfig = bData.GetConfigForLevel(buildingLevel);

            // check if time is valid
            if (!TimeIsValid(playerUpgradeDate.AddSeconds(-bConfig.duration), 50000, out double difference))
            {
                fLogger.LogError("Player submitted time is not valid. Difference to internal date is:" + difference + " ms");
                return resyncRequest;
            }

            // check if player can afford the unit
            if (!await CheckAndSubtractBuildingUpgradeCost(playerID, buildingID, buildingLevel, mainBuildingLevel, fLogger))
            {
                fLogger.LogError("Player cannot afford to upgrade building \"" + buildingID + "\" to level: " + (buildingLevel + 1).ToString() + " or the main building level is not high enough");
                return resyncRequest;
            }

            // save upgrade start date
            await PFCommunication.UpdateItemCustomData(playerID, bcID, buildingItemInstanceID, new Dictionary<string, string>() { { "UpgradeStartedAt", JsonConvert.SerializeObject(playerUpgradeDate) } }, fLogger);       

            fLogger.LogAll();
            return new Dictionary<string, string>() { { "DoResync", "false" } };
        }

        [FunctionName("FinishUpgradeBuilding")]
        public static async Task<dynamic> FinishUpgradeBuilding(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            FunctionLogger fLogger = new FunctionLogger("FinishUpgradeBuilding", playerID, true);
            if (!ArgsAreValid(context.FunctionArgument, "BuildingID", "Time", "SkipWithHC", "ExpectedHCCost"))
            {
                fLogger.LogError("Arguments are not valid");
                return resyncRequest;
            }
            dynamic args = context.FunctionArgument;

            // required data        
            string buildingId = args["BuildingID"];                                             // ID of the building
            string bcID = await PFCommunication.GetCharacterID(playerID, "Buildings", fLogger); // ID of the buildings character
            bool skipWithHC = JsonConvert.DeserializeObject<bool>(((string)args["SkipWithHC"]));
            int expectedHCCost = JsonConvert.DeserializeObject<int>(((string)args["ExpectedHCCost"]));

            // get all buildings 
            var allBuildings = await PFCommunication.GetCharacterInventoryByName(playerID, "Buildings", fLogger);

            // get building information
            string buildingItemInstanceID = "";
            int curLevel = 0;
            DateTime upgradeStartedAt = default;
            foreach (var curBuilding in allBuildings)
            {
                if (curBuilding.ItemId == buildingId)
                {
                    buildingItemInstanceID = curBuilding.ItemInstanceId;
                    curLevel = int.Parse(curBuilding.CustomData["Level"]);
                    try
                    {
                        upgradeStartedAt = JsonConvert.DeserializeObject<DateTime>(curBuilding.CustomData["UpgradeStartedAt"]);
                    }
                    catch
                    {
                        return new Dictionary<string, string>() { { "DoResync", "true" } };
                    }

                    break;
                }
            }

            // skip using HC
            if (skipWithHC)
            {
                var playerCurrencies = await PFCommunication.GetPlayerCurrencies(playerID, fLogger);

                var timeSpan = (upgradeStartedAt - DateTime.UtcNow);
                GameDataManager.GetDataForTableElement<GlobalVariables>("UpgradeSecondsPerHC", out GlobalVariables secondsPerHC);
                int hcCost = (int)timeSpan.TotalSeconds / int.Parse(secondsPerHC.Value);

                // check if player amount is valid  (max difference of 2 is allowed)
                if (MathF.Abs(hcCost - expectedHCCost) > 2)
                {
                    fLogger.LogError("Player expected HC cost is not valid. He expected " + expectedHCCost + " but " + hcCost + " were calculated");

                    return resyncRequest;
                }

                // check if player can afford
                if (playerCurrencies["HC"] >= expectedHCCost)
                {
                    fLogger.AddMessage("Player expected HC cost was valid");

                    if (expectedHCCost > 0)
                        await PFCommunication.SubstractCurrencyFromPlayer(playerID, "HC", expectedHCCost, fLogger);
                }
                else
                {
                    fLogger.LogError("Player does not have enough HC to skip the upgrade! Required is: " + expectedHCCost + " but he only has " + playerCurrencies["HC"]);

                    return resyncRequest;
                }
            }
            // check if time is valid
            else
            {
                // time difference is too big to finish upgrade -> resync
                if (!TimeIsValid(upgradeStartedAt, 50000, out double difference))
                {
                    LoggingHandler.Log("Cannot finish upgrading building '" + buildingId + "'. " +
                                        "\nUpgrade should be finished at: " + upgradeStartedAt.ToString() +
                                        "\nBut now is " + DateTime.Now.ToUniversalTime().ToString(),
                                        LogType.Warning);

                    return resyncRequest;
                }
                // upgrade should already be finished
                else
                {
                    fLogger.AddMessage("Finishing upgrade was at valid time.");
                }
            }

            // set new level
            await PFCommunication.UpdateItemCustomData(playerID, bcID, buildingItemInstanceID,
                new Dictionary<string, string>()
                {
                    { "Level", (curLevel + 1).ToString() },
                    { "UpgradeStartedAt", "null"},
                    { "LastCollectedAt", JsonConvert.SerializeObject(upgradeStartedAt)}
                }, fLogger);

            // todo: check if upgrade time is really over
            fLogger.LogAll();
            return new Dictionary<string, string>() { { "DoResync", "false" } };
        }
        #endregion

        #region Managing Units
        [FunctionName("PurchaseUnit")]
        public static async Task<dynamic> PurchaseUnit(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            FunctionLogger fLogger = new FunctionLogger("PurchaseUnit", playerID, true);
            if (!ArgsAreValid(context.FunctionArgument, "UnitID", "Time", "Amount"))
            {
                fLogger.LogError("Arguments are not valid");
                return resyncRequest;
            }
            dynamic args = context.FunctionArgument;
           
            // required data
            string unitID = args["UnitID"];                                                         // ID of the unit
            string unitAmountAsString = args["Amount"];                                             // amount to purchase raw
            int unitAmount = int.Parse(unitAmountAsString);                                         // amount to purchase
            string dateAsString = args["Time"];                                                     // date when player started raw
            DateTime playerPurchaseDate = default;                                                  // date when player started
            try { playerPurchaseDate = JsonConvert.DeserializeObject<DateTime>(dateAsString); }
            catch (Exception e) { fLogger.LogError("Cannot deserialize player submitted time: " + e.Message); return resyncRequest; }
            if (!TimeIsValid(playerPurchaseDate, 50000, out double differenceMS))
            {
                fLogger.LogError("Player submitted time is not valid. Difference to internal date is:" + differenceMS + " ms");
                return resyncRequest;
            }                // check if time is valid
            var allIDs = await PFCommunication.GetCharacterIDs(playerID, fLogger);                  // get all IDs of the characters 
            string utcID = allIDs["UnitsOwned"];                                                    // ID of the units training character //todo : implement training -> character is UnitsTraining

            // get all buildings
            var allBuildings = await PFCommunication.GetCharacterInventoryByID(playerID, allIDs["Buildings"], fLogger);

            // get baracks level
            int baracksLevel = -1;
            foreach (var curBuilding in allBuildings)
            {
                if (curBuilding.ItemId == "building_07")
                {
                    baracksLevel = int.Parse(curBuilding.CustomData["Level"]);
                    break;
                }
            }

            // check if player can afford the unit
            if (!await CheckAndSubtracPurchasetUnitCost(playerID, unitID, unitAmount, baracksLevel, fLogger))
            {
                fLogger.LogError("Player does not have enough currency to purchase unit \"" + unitID + "\" or the barracks level is not high enough");
                return resyncRequest;
            }

            // grant units           
            var t_GrantUnits = new List<Task<GrantItemsToCharacterResult>>();
            Task t_InitUnits = null;

            // grant items 
            for (int i = 0; i < unitAmount; i++)
            {
                t_GrantUnits.Add(PFCommunication.GrantItemsToCharacter(playerID, utcID, "Units", new List<string> { unitID }, fLogger));
                await Task.Delay(60);
            }

            // wait for granting
            while (t_GrantUnits.Count > 0)
            {
                var finishedTask = await Task.WhenAny(t_GrantUnits);

                // skipp  
                if (t_GrantUnits.Count > 1)
                {
                    t_GrantUnits.Remove(finishedTask);
                    continue;
                }

                var grantResult = finishedTask.Result;

                // save upgrade start date
                t_InitUnits = PFCommunication.UpdateItemCustomData(playerID, utcID, grantResult.ItemGrantResults[0].ItemInstanceId,
                    new Dictionary<string, string>
                    {
                    { "StartedTrainingAt", JsonConvert.SerializeObject(playerPurchaseDate) },
                    }, fLogger);

                break;
            }

            // wait initialization
            await t_InitUnits;

            fLogger.LogAll();
            return new Dictionary<string, string>() { { "DoResync", "false" } };
        }

        [FunctionName("StartUpgradeUnit")]
        public static async Task<dynamic> StartUpgradeUnit(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            FunctionLogger fLogger = new FunctionLogger("StartUpgradeUnit", playerID, true);
            if (!ArgsAreValid(context.FunctionArgument, "UnitID", "Time"))
            {
                fLogger.LogError("Arguments are not valid");
                return resyncRequest;
            }
            dynamic args = context.FunctionArgument;

            // required data
            string unitID = args["UnitID"];                                                         // ID of the unit
            string dateAsString = args["Time"];                                                     // date when player started raw
            DateTime playerUpgradeDate = default;                                                   // date when player started
            try { playerUpgradeDate = JsonConvert.DeserializeObject<DateTime>(dateAsString); }
            catch (Exception e) { fLogger.LogError("Cannot deserialize player submitted time: " + e.Message); return resyncRequest; }
            if (!TimeIsValid(playerUpgradeDate, 50000, out double differenceMS))
            {
                fLogger.LogError("Player submitted time is not valid. Difference to internal date is:" + differenceMS + " ms");
                return resyncRequest;
            }                 // check if time is valid        

            // get current unit levels
            var userData = await PFCommunication.GetUserData(playerID, new List<string>() { "UnitLevels", "UpgradingUnits" }, PlayFabDataType.Internal, fLogger);
            var unitLevels = JsonConvert.DeserializeObject<Dictionary<string, int>>(userData["UnitLevels"].Value);

            // get current level of unit
            int curLevel = -1;
            try { curLevel = unitLevels[unitID]; }
            catch { fLogger.LogError("Cannot find level etry of unit \"" + unitID + "\" in dictionary at title data"); return resyncRequest; }

            // check and subtract ugprade cost 
            if (!await CheckAndSubtractUnitUpgradeCost(playerID, unitID, curLevel, fLogger))
            {
                fLogger.LogError("Player cannot afford to upgrade unit \"" + unitID + "\". to level: " + (curLevel + 1).ToString());
                return resyncRequest;
            }

            // increase level -> delete if upgrading is implemented 
            unitLevels[unitID]++;

            // increase level immediately
            await PFCommunication.UpdateUserData(playerID, new Dictionary<string, string>() { { "UnitLevels", JsonConvert.SerializeObject(unitLevels) } }, PlayFabDataType.Internal, fLogger);

            // todo: implement upgrading in unity -> then add this functionarly instead
            /*
            // mark unit as upgraded 
            var upgradingUnits = JsonConvert.DeserializeObject<Dictionary<string, string>>(userData["UpgradingUnits"].Value);
            upgradingUnits.Add(unitID, dateAsString);
            await PFCommunication.UpdateUserData(playerID, new Dictionary<string, string>() { { "UpgradingUnits", JsonConvert.SerializeObject(upgradingUnits) } }, PlayFabDataType.Internal, fLogger);
            
            fLogger.Add("Started upgrade for unit \"" + unitID + "\");
            */

            fLogger.LogAll("Started upgrade for unit \"" + unitID + "\" to level: " + (curLevel + 1).ToString());
            return new Dictionary<string, string>() { { "DoResync", "false" } };
        }

        [FunctionName("RemoveUnit")]
        public static async Task RemoveUnit(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context 
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            dynamic args = context.FunctionArgument;
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            FunctionLogger fLogger = new FunctionLogger("RemoveUnit", playerID, true);

            // required data
            string unitID = args["unitID"];         // ID of the unit
            string unitState = args["unitState"];   // name of the character

            // get inventory
            var allItems = await PFCommunication.GetCharacterInventoryByName(playerID, unitState, fLogger);

            // search and remove item
            foreach (var curItem in allItems)
            {
                if (curItem.ItemId == unitID)
                {
                    // remove item 
                    await PFCommunication.RemoveItemsFromCharacter(playerID, unitState, curItem.ItemInstanceId, 1, fLogger);
                    break;
                }
            }

            // todo: check and pay resources?
            fLogger.LogAll();
        }
        #endregion

        #region Match Making
        [FunctionName("FindRandomEnemy")]
        public static async Task<dynamic> FindRandomEnemy(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;

            var fLogger = new FunctionLogger("FindRandomEnemy", playerID, true);

            // get laste enemy ID 
            var t_GetAllData = PFCommunication.GetUserData(playerID, new List<string>() { "LastEnemyIDs", "UnitLevels", "ArmySetups" }, PlayFabDataType.Internal, fLogger);

            // get player acccount info
            var t_GetAccountInfo = PFCommunication.GetUserAccountInfo(playerID, fLogger);
            var t_GetPlayerCurrencies = PFCommunication.GetPlayerCurrencies(playerID, fLogger);

            // wait for the player data
            var playerAllData = await t_GetAllData;
            var playerAcountInfo = await t_GetAccountInfo;
          
            // get last enemy, player unit levels and setups
            var playerLastEnemyIDs = JsonConvert.DeserializeObject<Queue<string>> (playerAllData["LastEnemyIDs"].Value);
            var playerName = playerAcountInfo.UserInfo.TitleInfo.DisplayName;
            Random rnd = new Random();

            // try to find enemy 3 times
            for (int range = 20; range <= 100; range += 20)
            {
                // get leaderboard 
                var possibleEnemies = await PFCommunication.GetLeaderboardAroundPlayer(playerID, "Battle Points", range, fLogger);

                // search for possible enemy
                for (int j = 0; j < 100; j++)
                {
                    // cannot find any enemy?
                    if (possibleEnemies.Count == 0)
                    {
                        fLogger.AddMessage("Cannot find enemy with " + j + " tries at range " + range);

                        break;
                    }

                    int enemyIndex = rnd.Next(0, possibleEnemies.Count);
                    var chosenPlayer = possibleEnemies[enemyIndex];
                    string enemyID = chosenPlayer.PlayFabId;
                    string enemyName = chosenPlayer.DisplayName;

                    // already fought against player recently
                    if (playerLastEnemyIDs.Contains(enemyID) ||enemyID == playerID)
                    {
                        possibleEnemies.RemoveAt(enemyIndex);
                        continue;
                    }

                    // get army setup & unit level from enemy
                    var enemyAllData = await PFCommunication.GetUserData(enemyID, new List<string> { "UnitLevels", "ArmySetups" }, PlayFabDataType.Internal, fLogger);

                    // find active setups
                    ArmySetup enemyActiveSetup = null;
                    foreach (var curSetup in JsonConvert.DeserializeObject<List<ArmySetup>>(enemyAllData["ArmySetups"].Value))
                    {
                        if (curSetup.isActive)
                            enemyActiveSetup = curSetup;
                    }

                    // no active setup? (should never happen)
                    if (enemyActiveSetup == null)
                    {
                        fLogger.AddMessage("Cannot find active army setup on player \"" + enemyName + "\" (" + enemyID + ".)");
                        possibleEnemies.RemoveAt(enemyIndex);
                        continue;
                    }

                    // setup has no troops?
                    if (enemyActiveSetup.placedUnits.Count + enemyActiveSetup.placedHeroes.Count == 0)
                    {
                        fLogger.AddMessage("Army setup of \"" + enemyName + "\" (" + enemyID + ") does not contain any troops");
                        possibleEnemies.RemoveAt(enemyIndex);
                        continue;
                    }

                    // log tries
                    fLogger.AddMessage("Found enemy \"" + enemyName + "\" (" + enemyID + ") with " + j + " tries at range " + range);

                    // calculate enemy data
                    EnemyData ed = new EnemyData()
                    {
                        name = enemyName,
                        UniqueId = enemyID,
                        armySetup = enemyActiveSetup,
                        unitLevels = JsonConvert.DeserializeObject<Dictionary<string, int>>(enemyAllData["UnitLevels"].Value),
                        reward = CalculateMatchReward(),
                    };

                    // store match 
                    var match = new PlayerMatch(playerID, playerName, enemyID, enemyName, ed.reward, null, DateTime.UtcNow);

                    // is there already a match running?
                    if (currentMatches.ContainsKey(playerID))
                    {
                        fLogger.AddMessage("There is already a match running with \"" + currentMatches[playerID].defenderID + "\". Match has been removed");
                        currentMatches.Remove(playerID);
                    }

                    // add match to cache
                    currentMatches.Add(playerID, match);

                    // check if player can afford 
                    var playerCurrencies = await t_GetPlayerCurrencies;

                    if (playerCurrencies["FO"] < 100)
                    {
                        fLogger.LogError("Player cannot afford matches. He only has " + playerCurrencies["FO"] + " food");

                        return new Dictionary<string, string>()
                        {
                            { "DoResync", "true" },
                            { "EnemyData", null},
                        };
                    }

                    // subtract currency
                    var t_Subtractcurrency = PFCommunication.SubstractCurrencyFromPlayer(playerID, "FO", 100, fLogger);

                    // enqueue new enemy
                    playerLastEnemyIDs.Enqueue(enemyID);
                    while (playerLastEnemyIDs.Count > 5)
                        playerLastEnemyIDs.Dequeue();

                    // save enemy ID 
                    var t_UpdateUserData = PFCommunication.UpdateUserData(playerID, new Dictionary<string, string>() { { "LastEnemyIDs", JsonConvert.SerializeObject(playerLastEnemyIDs) } }, PlayFabDataType.Internal, fLogger);

                    await t_Subtractcurrency;
                    await t_UpdateUserData;

                    fLogger.LogAll();
                    return new Dictionary<string, string>()
                        {
                        { "DoResync", "false" },
                        { "EnemyData", JsonConvert.SerializeObject(ed) },
                    };
                }
            }

            // cannot find any enemy 
            return new Dictionary<string, string>()
            {
                { "DoResync", "false" },
                { "EnemyData", null},
            };
        }

        [FunctionName("AbortMatch")]
        public static async Task<dynamic> AbortMatch(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            FunctionLogger fLogger = new FunctionLogger("AbortMatch", playerID, true);

            currentMatches.Remove(playerID);
            fLogger.LogAll();

            return new Dictionary<string, string>(){ { "DoResync", "false" } };
        }

        [FunctionName("FinishedMatch")]
        public static async Task<dynamic> FinishedMatch(
        [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
        ILogger log)
        {
            await SetStaticData(log);

            // get context & player
            var context = await PFCommunication.GetFunctionExecutionContext(req);
            string playerID = context.CallerEntityProfile.Lineage.MasterPlayerAccountId;
            FunctionLogger fLogger = new FunctionLogger("FinishedMatch", playerID, true);
            if (!ArgsAreValid(context.FunctionArgument, "Result"))
            {
                fLogger.LogError("Arguments are not valid");
                return resyncRequest;
            }
            dynamic args = context.FunctionArgument;

            // required data
            string resultAsString = args["Result"];
            var attackerWon = bool.Parse(resultAsString);
            var match = currentMatches[playerID];
            match.InitializeResult(attackerWon);

            // grant match reward 
            var t_GrantRewards= new List<Task>();
            if (attackerWon)
            {
                t_GrantRewards.Add (GrantMatchRewardToPlayer(match.attackerID, match, fLogger));
                t_GrantRewards.Add (SubtractMatchRewardFromPlayer(match.defenderID, match, fLogger));
            }
            else
            {
                t_GrantRewards.Add (GrantMatchRewardToPlayer(match.defenderID, match, fLogger));
                t_GrantRewards.Add (SubtractMatchRewardFromPlayer(match.attackerID, match, fLogger));
            }

            // get defender offline matches
            var allUserData = await PFCommunication.GetUserData(match.defenderID,
                new List<string>()
                { "OfflineMatches" },
                PlayFabDataType.Internal,
                fLogger);

            // wait for granting rewards
            await Task.WhenAll(t_GrantRewards);

            // offline machtes as list
            var defenderOfflineMatches = JsonConvert.DeserializeObject<List<PlayerMatch>>(allUserData["OfflineMatches"].Value);
            defenderOfflineMatches.Add(match);

            // save match in defender user data
            await PFCommunication.UpdateUserData(match.defenderID,
                new Dictionary<string, string>()
                {
                    { "OfflineMatches", JsonConvert.SerializeObject(defenderOfflineMatches) },
                },
                PlayFabDataType.Internal,
                fLogger);

            // remove match
            currentMatches.Remove(playerID);

            fLogger.LogAll(true);
            return new Dictionary<string, string>()
            {
                { "DoResync", "false" },
            };
        }

        public class PlayerMatch
        {
            public string attackerID { get; private set; }
            public string attackerDisplayName { get; private set; }
            public string defenderID { get; private set; }
            public string defenderDisplayName { get; private set; }
            public MatchReward reward { get; private set; }
            public MatchResult result { get; private set; }
            public DateTime startedAt { get; private set; }

            public PlayerMatch(string attackerID, string attackerDisplayName, string defenderID, string defenderDisplayName, MatchReward reward, MatchResult result, DateTime startedAt)
            {
                this.attackerID = attackerID;
                this.attackerDisplayName = attackerDisplayName;
                this.defenderID = defenderID;
                this.defenderDisplayName = defenderDisplayName;
                this.reward = reward;
                this.result = result;
                this.startedAt = startedAt;
            }

            public void InitializeResult(bool attackerWon)
            {
                result = new MatchResult(attackerWon, 0, 0);
            }
        }

        public class MatchReward
        {
            public Dictionary<Resource, int> resources { get; private set; }
            public int xp { get; private set; }

            public MatchReward(Dictionary<Resource, int> resources, int xp)
            {
                this.resources = resources;
                this.xp = xp;
            }
        }

        public class MatchResult
        {
            public bool attackerWon { get; private set; }
            public int attackerBPChange { get; private set; }
            public int defenderBPChange { get; private set; }

            public MatchResult(bool attackerWon, int attackerBPChange, int defenderBPChange)
            {
                this.attackerWon = attackerWon;
                this.attackerBPChange = attackerBPChange;
                this.defenderBPChange = defenderBPChange;
            }

            public void SetWinner(bool attackerWon)
            {
                this.attackerWon = attackerWon;
            }

            public void SetAttackerBPChange(int amount)
            {
                this.attackerBPChange = amount;
            }

            public void SetDefenderBPChange(int amount)
            {
                this.defenderBPChange = amount;
            }
        }
        #endregion

        #region Helper Methods
        static async Task SetStaticData(ILogger useLogger)
        {
            PlayFabSettings.staticSettings.TitleId = "27F34";
            PlayFabSettings.staticSettings.DeveloperSecretKey = "BUSJPPOYJK8AYC4F9ZDNP3R6MKFYET1WDJHPWU9FS1YJQANT1S";
            LoggingHandler.Initialize(useLogger);

            await GameDataManager.LoadTable<BuildingData>();
            await GameDataManager.LoadTable<UnitData>();
            await GameDataManager.LoadTable<GlobalVariables>();
        }

        private static bool TimeIsValid(DateTime submittedTime, int maxMSDifference, out double calculatedDifference)
        {
            var playerDateUTC = TimeZoneInfo.ConvertTimeToUtc(submittedTime);
            var internalDateUTC = TimeZoneInfo.ConvertTimeToUtc(DateTime.Now);

            var timeSpan = internalDateUTC - playerDateUTC;

            calculatedDifference = Math.Abs (timeSpan.TotalMilliseconds);
            return calculatedDifference < maxMSDifference ? true : false;
        }

        private static double TimeDifferenceMS(DateTime earlyDate, DateTime lateDate)
        {
            return (TimeZoneInfo.ConvertTimeToUtc(lateDate) - TimeZoneInfo.ConvertTimeToUtc(earlyDate)).TotalMilliseconds;
        }

        private static bool StringIsNotNullOrEmpty(string input) 
        {
            if (string.IsNullOrEmpty(input) || input == "null")
                return true;

            return false;
        }

        private static bool ArgsAreValid(dynamic args, params string[] requiredKeys)
        {
            foreach (var currentKey in requiredKeys)
            {
                try 
                {
                    var c = args[currentKey];
                }
                catch
                {
                    return false;
                }
            }

            return true;
        }

        private static Dictionary<string, int> CalculateProducedResources(string playerID, string buildingID, int buildingLevel, int secondsProduced) 
        {
            // request the building data for the building
            GameDataManager.GetDataForTableElement<BuildingData>(buildingID, out BuildingData bData);

            // get config 
            var config = bData.GetConfigForLevel(buildingLevel);

            // calculate amount 
            var amount = secondsProduced * config.production / 3600;

            // clamp amount
            amount = Math.Clamp(amount, 0, config.capacity);

            return config.resource != 0? new Dictionary<string, int> () { { ((Resource) config.resource).ToString(), amount } } : null;
        }

        private static async Task<bool> CheckAndSubtracPurchasetUnitCost (string playerID, string unitID, int amount, int barracksLevel, FunctionLogger fLogger)
        {
            // request the building data for the building
            GameDataManager.GetDataForTableElement<UnitData>(unitID, out UnitData uData);
            GameDataManager.GetDataForTableElement<BuildingData>("building_07", out BuildingData barracksData); // todo: store such important IDs somewhere and save them in title data

            // check if barracks level is high enough
            if (barracksData.value3s == null)
                barracksData.GetConfigForLevel(1);
            var unitTypeToUnlockLevel = new Dictionary<string, int>();
            for (int i = 0; i < barracksData.value3s.Count; i++)
            {
                string[] unlockedUnitsAtLevel = barracksData.value3s[i].Split(',');

                for (int j = 0; j < unlockedUnitsAtLevel.Length; j++)
                {
                    unitTypeToUnlockLevel.Add(unlockedUnitsAtLevel[j], i + 1);
                }
            }
            if (barracksLevel < unitTypeToUnlockLevel[unitID])
                return false;

            // check if player can afford it 
            var allCurrencies = await PFCommunication.GetPlayerCurrencies(playerID, fLogger);

            // get costs
            var costWood = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costWood, '\n')[0] * amount;
            var costStone = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costStone, '\n')[0] * amount;
            var costIron = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costIron, '\n')[0] * amount;
            var costLeather = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costLeather, '\n')[0] * amount;
            var costFood = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costFood, '\n')[0] * amount;

            // check
            {
                // check wood 
                if (allCurrencies["WO"] < costWood)
                {
                    return false;
                }
                // check stone 
                if (allCurrencies["ST"] < costStone)
                {
                    return false;
                }
                // check iron 
                if (allCurrencies["IR"] < costIron)
                {
                    return false;
                }
                // check leather 
                if (allCurrencies["LE"] < costLeather)
                {
                    return false;
                }
                // check food 
                if (allCurrencies["FO"] < costFood)
                {
                    return false;
                }
            }

            // subtract currencies
            {
                List<Task> tasks = new List<Task>();

                if (costWood > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "WO", costWood, fLogger));

                if (costStone > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "ST", costStone, fLogger));

                if (costIron > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "IR", costIron, fLogger));
   
                if (costLeather > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "LE", costLeather, fLogger));

                if (costFood > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "FO", costFood, fLogger));

                await Task.WhenAll(tasks);
            }

            return true;
        }

        private static async Task<bool> CheckAndSubtractUnitUpgradeCost(string playerID, string unitID, int curLevel, FunctionLogger fLogger)
        {
            // request the building data for the building
            GameDataManager.GetDataForTableElement<UnitData>(unitID, out UnitData uData);

            // check if player can afford it 
            var allCurrencies = await PFCommunication.GetPlayerCurrencies(playerID, fLogger);

            // get costs
            var costWood = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costWood, '\n')[curLevel];
            var costStone = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costStone, '\n')[curLevel];
            var costIron = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costIron, '\n')[curLevel];
            var costLeather = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costLeather, '\n')[curLevel];
            var costFood = DeserializationHelper.ParseElementToListOfPrimitives<int>(uData.costFood, '\n')[curLevel];

            // check
            {
                // check wood 
                if (allCurrencies["WO"] < costWood)
                {
                    return false;
                }
                // check stone 
                if (allCurrencies["ST"] < costStone)
                {
                    return false;
                }
                // check iron 
                if (allCurrencies["IR"] < costIron)
                {
                    return false;
                }
                // check leather 
                if (allCurrencies["LE"] < costLeather)
                {
                    return false;
                }
                // check food 
                if (allCurrencies["FO"] < costFood)
                {
                    return false;
                }
            }

            // subtract currencies
            {
                List<Task> tasks = new List<Task>();

                if (costWood > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "WO", costWood, fLogger));

                if (costStone > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "ST", costStone, fLogger));

                if (costIron > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "IR", costIron, fLogger));

                if (costLeather > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "LE", costLeather, fLogger));

                if (costFood > 0)
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "FO", costFood, fLogger));

                await Task.WhenAll(tasks);
            }

            return true;
        }

        private static async Task<bool> CheckAndSubtractBuildingUpgradeCost(string playerID, string buildingID, int curLevel, int mainBuildingLevel, FunctionLogger fLogger)
        {
            // request the building data for the building
            GameDataManager.GetDataForTableElement<BuildingData>(buildingID, out BuildingData bData);
            var bconfig = bData.GetConfigForLevel(curLevel);

            // check if main building level is valid
            if (bconfig.mainBuildLvlReq > mainBuildingLevel)
                return false;

            // check if player can afford it 
            var allCurrencies = await PFCommunication.GetPlayerCurrencies(playerID, fLogger);

            // check
            {
                // check wood 
                if (allCurrencies["WO"] < bconfig.woodCost)
                {
                    return false;
                }
                // check stone 
                if (allCurrencies["ST"] < bconfig.stoneCost)
                {
                    return false;
                }
                if (allCurrencies["FO"] < bconfig.foodCost)
                {
                    return false;
                }
            }

            // subtract currencies
            {
                List<Task> tasks = new List<Task>();

                if (bconfig.woodCost > 0)
                {
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "WO", bconfig.woodCost, fLogger));
                }

                if (bconfig.stoneCost > 0)
                {
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "ST", bconfig.stoneCost, fLogger));
                }

                if (bconfig.foodCost > 0)
                {
                    tasks.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, "FO", bconfig.foodCost, fLogger));
                }

                await Task.WhenAll(tasks);
            }
            return true;
        }

        private static MatchReward CalculateMatchReward() 
        {
            Random rnd = new Random();

            MatchReward reward = new MatchReward(new Dictionary<Resource, int>() 
            {
                {Resource.WO, rnd.Next(10, 100) },
                {Resource.ST, rnd.Next(10, 100) },
                {Resource.IR, rnd.Next(10, 100) },
                {Resource.LE, rnd.Next(10, 100) },
                {Resource.FO, rnd.Next(10, 100) },
            }, 10);

            return reward;
        }

        private static async Task GrantMatchRewardToPlayer(string playerID, PlayerMatch match, FunctionLogger fLogger)
        {
            var reward = match.reward;

            // grant currencies
            var t_GrantCurrencies = new List<Task>();
            foreach (var currentResource in reward.resources.Keys)
            {
                if (reward.resources[currentResource] > 0)
                    t_GrantCurrencies.Add(PFCommunication.AddCurrencyToPlayer(playerID, currentResource.ToString(), reward.resources[currentResource], fLogger));
            }
            
            // grant 5 battle points
            await PFCommunication.UpdatePlayerStatistics(playerID,
                new List<StatisticUpdate>()
                {
                    new StatisticUpdate() 
                    { 
                        StatisticName = "Battle Points",
                        Value = 5,
                    },
                    new StatisticUpdate()
                    {
                        StatisticName = "XP",
                        Value = reward.xp,
                    }
                }, fLogger);

            // set result in object
            if (match.result.attackerWon)
                match.result.SetAttackerBPChange (5);
            else
                match.result.SetDefenderBPChange(5);

            await Task.WhenAll(t_GrantCurrencies);
        }

        private static async Task SubtractMatchRewardFromPlayer(string playerID, PlayerMatch match, FunctionLogger fLogger)
        {
            var reward = match.reward;

            // request statistics
            var t_GetPlayerStatistics = PFCommunication.GetPlayerStatistics(playerID, new List<string> { "Battle Points" }, fLogger);

            // grant currencies //todo: lose match reward?
            var t_SubtractReward = new List<Task>();
            /*

            foreach (var currentResource in reward.resources.Keys)
            {
                if (reward.resources[currentResource] > 0)
                    t_GrantCurrencies.Add(PFCommunication.SubstractCurrencyFromPlayer(playerID, currentResource.ToString(), reward.resources[currentResource], fLogger));
            }
            */
            // wait for statistics
            var battleStastics = await t_GetPlayerStatistics;
            var curBattlePoints = battleStastics[0].Value;

            // calculate loss
            int pointsToLose = 0;
            if (curBattlePoints >= 100)
                pointsToLose = 5;
            else if (curBattlePoints >= 60)
                pointsToLose = 4;
            else if (curBattlePoints >= 40)
                pointsToLose = 3;
            else if (curBattlePoints >= 20)
                pointsToLose = 2;
            else
                pointsToLose = 1;

            // subtract battle points
            t_SubtractReward.Add (PFCommunication.UpdatePlayerStatistics(playerID,
                new List<StatisticUpdate>()
                {
                    new StatisticUpdate()
                    {
                        StatisticName = "Battle Points",
                        Value = curBattlePoints >= pointsToLose? -pointsToLose : -curBattlePoints,
                    }
                }, 
                fLogger));

            // set result in object
            if (match.result.attackerWon)
                match.result.SetDefenderBPChange(pointsToLose);
            else
                match.result.SetAttackerBPChange(pointsToLose);

            await Task.WhenAll(t_SubtractReward);
        }
        #endregion
    }
}
  
