﻿using Microsoft.Extensions.Logging;
using PlayFab.AdminModels;
using System;
using System.Collections.Generic;
using System.Text;

namespace Cloud_Script_Functions
{
    public enum LogType { Log, Warning, Error }
    static class LoggingHandler
    {
        private static ILogger curLog;

        public static void Initialize(ILogger log)
        {
            curLog = log;
        }

        public static void Log(string message)
        {
            Log(message, LogType.Log);
        }

        public static void Log(string message, LogType type)
        {
            if (curLog == null)
            {
                Console.WriteLine("LoggingHandler was not initializes. Cannot log message: " + message);
                return;
            }

            switch (type)
            {
                case LogType.Log:
                    curLog.LogInformation(message);
                    break;
                case LogType.Warning:
                    curLog.LogWarning(message);
                    break;
                case LogType.Error:
                    curLog.LogError(message);
                    break;
            }
        }

        public static void LogFormat(LogType type, string message, params object[] args)
        {
            if (curLog == null)
            {
                Console.WriteLine("LoggingHandler was not initializes. Cannot log message: " + message);
                return;
            }

            switch (type)
            {
                case LogType.Log:
                    curLog.LogInformation(message, args);
                    break;
                case LogType.Warning:
                    curLog.LogWarning(message, args);
                    break;
                case LogType.Error:
                    curLog.LogError(message, args);
                    break;
            }
        }

        public static void MarkFunctionStart(string function)
        {
            if (curLog == null)
            {
                Console.WriteLine("LoggingHandler was not initializes. Cannot mark function start of " + function);
                return;
            }

            string mark = "\n////////////////////   ";
            mark += function + "   ";

            for (int i = mark.Length; i <= 60; i++)
                mark += "/";

            curLog.LogInformation(mark);
        }
    }
}
