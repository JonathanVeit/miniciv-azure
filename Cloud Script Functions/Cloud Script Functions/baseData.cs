﻿using Cloud_Script_Functions;
using StratosphereGames.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;

namespace StratosphereGames
{
    public interface IUniqueId
    {
        string UniqueId { get; set; }
    }


    public class BaseData : IUniqueId
    {
        private struct PowerKey
        {
            public int Level;
            public int Tier;
            public float PowerFactor;
        }

        private struct ResultKey
        {
            public float Base;
            public string Factor;
        }

        #region Base defintion data

        public readonly float LINEAR_CHANGE_PER_VALUE = 0.5f;
        public readonly float POWER_VALUE = 1.1f;

        protected enum LevelIncreaseType
        {
            Incremental,
            Power,
            Linear
        }

        #endregion

        // shared by all data elements
        public string UniqueId { get; set; }

        private static Dictionary<PowerKey, double> PowerCache = new Dictionary<PowerKey, double>();
        private static Dictionary<ResultKey, double> ResultCache = new Dictionary<ResultKey, double>();

        #region Helper Functions

        protected double GetLevelSpecificValue(float baseValue, int level, int tier = 0, 
                            float specificLinearFactor = 0.5f, float specificPowerFactor = 1.1f, 
                            bool roundToNearestInt = false, LevelIncreaseType type = LevelIncreaseType.Power)
        {
            PowerKey powerKey = new PowerKey
            {
                Level = level,
                Tier = tier,
                PowerFactor = specificLinearFactor
            };

            double result = baseValue;
            double totalFactor;

            if (type == LevelIncreaseType.Power && PowerCache.ContainsKey(powerKey))
            {
                totalFactor = PowerCache[powerKey];
                //Debug.Log("Power hit");
            }
            else
            {
                //Debug.Log("Power miss");
                double levelFactor = 1;
                float tierFactor = GetUnitTierFactor();

                switch (type)
                {
                    case LevelIncreaseType.Incremental:
                        result += (level - 1);
                        break;
                    case LevelIncreaseType.Power:
                        double powPerLevel = Math.Pow(specificPowerFactor, (level - 1));
                        double powPerTenthLevel = Math.Pow(1.25f, (level / 10));
                        levelFactor = powPerLevel * powPerTenthLevel;
                        break;
                    case LevelIncreaseType.Linear:
                        levelFactor = specificLinearFactor * (level - 1);
                        break;
                    default:
                        LoggingHandler.Log("baseData: Unhandled level increase type: " + type, LogType.Error);
                        result = 0;
                        break;
                }

                double tierBonus = Math.Pow(tierFactor, Math.Max(0, tier - 1));
                totalFactor = levelFactor * tierBonus;

                if (type == LevelIncreaseType.Power)
                {
                    PowerCache.Add(powerKey, totalFactor);
                }
            }

            ResultKey resultKey = new ResultKey
            {
                Base = baseValue,
                Factor = totalFactor.ToString()
            };

            if(ResultCache.ContainsKey(resultKey))
            {
                result = ResultCache[resultKey];
                //Debug.Log("Result hit");
            }
            else
            {
                //Debug.Log("Result miss");
                result *= totalFactor;
                ResultCache.Add(resultKey, result);
            }


            if (roundToNearestInt)
            {
                result = (int)(result + 0.5f);
            }

            return result;
        }

        protected virtual float GetUnitTierFactor()
        {
            // implement in inheriting class?
            // original: GlobalVariables.UnitTierFactor;
            return 1;
        }

        protected List<T> GetListFromValue<T>(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return new List<T>();
            }
            if (typeof(T) != typeof(string))
            {
                return DeserializationHelper.ParseElementToListOfPrimitives<T>(value);
            }
            else
            {
                return DeserializationHelper.SplitElementToStringList(value) as List<T>;
            }
        }

        #endregion
    }
}
