﻿using System.Collections.Generic;
using System;
using Cloud_Script_Functions;

namespace StratosphereGames.Helpers
{
    /// <summary>
    /// Helper class to serialize strings. Can split strings into lists and parse string-lists to specific types.
    /// </summary>
    public class DeserializationHelper
    {
        private static readonly char DEFAULT_ELEMENT_SPLIT_CHAR = ':';
        private static readonly char DEFAULT_LIST_SPLIT_CHAR = ':';

        /// <summary>
        /// Split string into 2D List of strings.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="listSeparator"></param>
        /// <param name="elementSeparator"></param>
        /// <returns></returns>
        public static List<List<string>> SplitElementToListOfStringLists(string str, 
                                                    char listSeparator = ',', char elementSeparator = ':')
        {
            var result = new List<List<string>>();
            var serializedElementLists = SplitElementToStringList(str, listSeparator);
            foreach (var serializedElementList in serializedElementLists)
            {
                result.Add(SplitElementToStringList(serializedElementList, elementSeparator));
            }

            return result;
        }

        /// <summary>
        /// Returns a 2D list of elements parsed from a given string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <returns></returns>
        public static List<List<T>> ParseElement2DListOfPrimitives<T>(string str)
        {
            var result = new List<List<T>>();
            List<List<string>> deserilaizedList = SplitElementToListOfStringLists(str, DEFAULT_LIST_SPLIT_CHAR, 
                                                                                    DEFAULT_ELEMENT_SPLIT_CHAR);
            foreach (var stringList in deserilaizedList)
            {
                result.Add(ParseStringListToListOfPrimitives<T>(stringList));
            }

            return result;
        }

        /// <summary>
        /// Parses a given string to a 2D List of element using parseFunc
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str"></param>
        /// <param name="parseFunc"></param>
        /// <returns></returns>
        public static List<List<T>> ParseElementTo2DList<T>(string str, Func<string, T> parseFunc)
        {
            var result = new List<List<T>>();
            List<List<string>> deserilaizedList = SplitElementToListOfStringLists(str, DEFAULT_LIST_SPLIT_CHAR,
                                                                                    DEFAULT_ELEMENT_SPLIT_CHAR);
            foreach (var stringList in deserilaizedList)
            {
                result.Add(ParseStringListToTypedList<T>(stringList, parseFunc));
            }

            return result;
        }

        /// <summary>
        /// Splits a given string element to a list of string using specificSeparatorSymbol as a split-indicator.
        /// </summary>
        /// <param name="originalString"></param>
        /// <param name="specificSeparatorSymbol"></param>
        /// <returns></returns>
        public static List<string> SplitElementToStringList(string originalString, char specificSeparatorSymbol = ':')
        {
            if(originalString == null)
            {
                return new List<string>();
            }
            string[] splitStrings = originalString.Split(specificSeparatorSymbol);

            return new List<string>(splitStrings);
        }

        /// <summary>
        /// Parses a given element, splitting it into a list and then using parseFunc to parse single
        /// elements to given type T.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="serializedList"></param>
        /// <param name="parseFunc">Anomynous function to parse a single element from string to T.</param>
        /// <returns></returns>
        public static List<T> ParseElementToList<T>(string serializedList, Func<string, T> parseFunc)
        {
            List<string> deserilaizedList = SplitElementToStringList(serializedList, DEFAULT_ELEMENT_SPLIT_CHAR);
            List<T> parsedList = ParseStringListToTypedList<T>(deserilaizedList, parseFunc);

            return parsedList;
        }

        /// <summary>
        /// Returns a parsed List from a given list of strings using ginve parseFunc.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stringList"></param>
        /// <param name="parseFunc"></param>
        /// <returns></returns>
        public static List<T> ParseStringListToTypedList<T>(List<string> stringList, Func<string, T> parseFunc)
        {
            List<T> result = new List<T>();
            foreach (string serializedElement in stringList)
            {
                T deserializedElement;
                try
                {
                    deserializedElement = parseFunc(serializedElement);
                }
                catch (Exception e)
                {
                    //DebugHelper.PrintFormatted(LogType.Error, "DeserializationHelper: Parsing string '{0}' to type {1} failed! Exception: {2}", serializedElement, typeof(T).ToString(), e.ToString());
                    continue;
                }
                result.Add(deserializedElement);
            }

            return result;
        }

        /// <summary>
        /// Parses an element into a list of elements of type T. T has to be a primitve or enum.
        /// </summary>
        /// <typeparam name="T">Has to be primitive type or enum.</typeparam>
        /// <param name="serializedList"></param>
        /// <returns></returns>
        public static List<T> ParseElementToListOfPrimitives<T>(string serializedList, char splitchar = char.MinValue)
        {
            if (splitchar == char.MinValue) splitchar = DEFAULT_ELEMENT_SPLIT_CHAR;
            List<string> deserilaizedList = SplitElementToStringList(serializedList, splitchar);
            List<T> parsedList = ParseStringListToListOfPrimitives<T>(deserilaizedList);

            return parsedList;
        }

        /// <summary>
        /// Returns parsed List from a List of strings representing elements of parsing type.
        /// </summary>
        /// <typeparam name="T">Type to parse to. Can either be bool, float or int. </typeparam>
        /// <param name="stringList"></param>
        /// <returns></returns>
        public static List<T> ParseStringListToListOfPrimitives<T>(List<string> stringList)
        {
            if (!typeof(T).IsPrimitive && !typeof(T).IsEnum && (typeof(T) !=  typeof(string)))
            {
                LoggingHandler.LogFormat(LogType.Error, "DeserializationHelper: Given type {0} is not a primtive!", typeof(T).ToString());
                return new List<T>();
            }

            List<object> parsedList = new List<object>();
            if (typeof(T) == typeof(string))
            {
                for (int i = 0; i < stringList.Count; i++)
                {
                    parsedList.Add(stringList[i]);
                }

            }
            else if (typeof(T) == typeof(int))
            {
                for (int i = 0; i < stringList.Count; i++)
                {
                    parsedList.Add(int.Parse(stringList[i]));
                }

            }
            else if (typeof(T) == typeof(long))
            {
                for (int i = 0; i < stringList.Count; i++)
                {
                    parsedList.Add(long.Parse(stringList[i]));
                }
            }
            else if (typeof(T) == typeof(float))
            {
                for (int i = 0; i < stringList.Count; i++)
                {
                    parsedList.Add(float.Parse(stringList[i]));
                }
            }
            else if (typeof(T) == typeof(bool))
            {
                for (int i = 0; i < stringList.Count; i++)
                {
                    parsedList.Add(bool.Parse(stringList[i]));
                }
            }
            else if (typeof(T).IsEnum)
            {
                for (int i = 0; i < stringList.Count; i++)
                {
                    parsedList.Add(EnumHelper.ParseValue<T>(stringList[i]));
                }
            }
            else
            {
                LoggingHandler.LogFormat(LogType.Error, "DeserializationHelper: Unhandled parse type: " + typeof(T));
                return new List<T>();
            }

            List<T> list = new List<T>();
            foreach (var element in parsedList)
            {
                T value = (T)element;
                list.Add(value);
            }

            return list;
        }

        public static bool TryParseValueAsPrimitive<T>(object val, out T parseResult)
        {
            parseResult = default(T);
            bool resultSucc = true;
            try
            {
                object parsedVal = null;
                if (typeof(T) == typeof(float))
                {
                    parsedVal = float.Parse(val.ToString());
                }
                else if (typeof(T) == typeof(int))
                {
                    parsedVal = int.Parse(val.ToString());
                }
                else if (typeof(T) == typeof(long))
                {
                    parsedVal = long.Parse(val.ToString());
                }
                else if (typeof(T) == typeof(bool))
                {
                    parsedVal = bool.Parse(val.ToString());
                }
                else if (typeof(T) == typeof(string))
                {
                    parsedVal = val.ToString();
                }
                parseResult = (T)parsedVal;
            }
            catch (Exception e)
            {
                resultSucc = false;
            }

            return resultSucc;
        }

        /// <summary>
        /// Returns parsed int from a strings representing a duration in seconds.
        /// </summary>
        /// <returns></returns>
        public static int ParseStringIntoSeconds(string serializedDuration)
        {
            return ParseStringIntoSeconds(serializedDuration, 'd', 'h', 'm', 's');
        }

        /// <summary>
        ///Returns parsed int from a strings representing a duration in seconds.
        /// </summary>
        /// <returns></returns>
        public static int ParseStringIntoSeconds(string serializedDuration, char daysMarker, char hoursMarker, char minutesMarker, char secondsMarker)
        {
            int seconds = 0;

            while (serializedDuration.Length > 0)
            {
                // days
                var index = serializedDuration.IndexOf(daysMarker);

                if (index >= 0)
                {
                    string[] parts = serializedDuration.Split(daysMarker);

                    seconds += int.Parse(parts[0]) * 86400;
                    serializedDuration = parts[1];
                    continue;
                }

                // hours
                index = serializedDuration.IndexOf(hoursMarker);

                if (index >= 0)
                {
                    string[] parts = serializedDuration.Split(hoursMarker);

                    seconds += int.Parse(parts[0]) * 3600;
                    serializedDuration = parts[1];
                    continue;
                }

                // minutes
                index = serializedDuration.IndexOf(minutesMarker);

                if (index >= 0)
                {
                    string[] parts = serializedDuration.Split(minutesMarker);

                    seconds += int.Parse(parts[0]) * 60;
                    serializedDuration = parts[1];
                    continue;
                }

                // seconds
                index = serializedDuration.IndexOf(secondsMarker);

                if (index >= 0)
                {
                    string[] parts = serializedDuration.Split(secondsMarker);

                    seconds += int.Parse(parts[0]);
                    serializedDuration = parts[1];
                    continue;
                }

                break;
            }

            return seconds;
        }
    }

}