﻿using StratosphereGames;
using StratosphereGames.Helpers;
using System.Collections.Generic;
using System.Linq;

namespace MiniCiv
{
    public enum Resource
    {
        NONE = 00,
        WO = 01, // Wood
        ST = 02, // Stone
        IR = 03, // Metal
        LE = 04, // Leather
        FO = 05, // Food
    }

    public enum BuildingType
    {
        None = 00,
        Farm = 01,
        Woodcutter = 02,
        Quarry = 03,
        Mine = 04,
        Huntsman = 05,
        Storehouse = 06,
        Barracks = 07,
        LivingQuarters = 08,
        MainBuilding = 09,
        PlaceUnits = 999999
    }

    public class BuildingData : BaseData
    {
        public string buildingId => UniqueId;
        public string name { get; set; }
        public int resource { get; set; }
        public string production { get; set; }
        public string capacity { get; set; }
        public string mainBuildLvlReq { get; set; }
        public string foodCost { get; set; }
        public string woodCost { get; set; }
        public string stoneCost { get; set; }
        public string duration { get; set; }
        public string value1 { get; set; }
        public string value2 { get; set; }
        public string value3 { get; set; }
        public string maxFood { get; set; }
        public string maxWood { get; set; }
        public string maxStone { get; set; }
        public string maxMetal { get; set; }
        public string maxLeather { get; set; }

        public List<int> productions { get; set; }
        public List<int> capacitys { get; set; }
        public List<int> mainBuildLvlReqs { get; set; }
        public List<int> foodCosts { get; set; }
        public List<int> woodCosts { get; set; }
        public List<int> stoneCosts { get; set; }
        public List<string> durations { get; set; }
        public List<float> value1s { get; set; }
        public List<float> value2s { get; set; }
        public List<string> value3s { get; set; }
        public List<int> maxFoods { get; set; }
        public List<int> maxWoods { get; set; }
        public List<int> maxStones { get; set; }
        public List<int> maxMetals { get; set; }
        public List<int> maxLeathers { get; set; }

        private Dictionary<int, BuildingConfig> ConfigForLevel = new Dictionary<int, BuildingConfig>();

        public BuildingConfig GetConfigForLevel(int level)
        {
            if (ConfigForLevel.ContainsKey(level))
            {
                return ConfigForLevel[level];
            }

            BuildingConfig buildingConfig = new BuildingConfig();
            buildingConfig.buildingId = UniqueId;
            buildingConfig.level = level;
            buildingConfig.resource = resource;

            if (!string.IsNullOrEmpty(production))
            {
                productions = DeserializationHelper.ParseElementToListOfPrimitives<int>(production, '\n');
                if (productions.Count >= level)
                {
                    buildingConfig.production = productions[level - 1];
                }
            }

            if (!string.IsNullOrEmpty(capacity))
            {
                capacitys = DeserializationHelper.ParseElementToListOfPrimitives<int>(capacity, '\n');
                if (capacitys.Count >= level)
                {
                    buildingConfig.capacity = capacitys[level - 1];
                }
            }

            if (!string.IsNullOrEmpty(mainBuildLvlReq))
            {
                mainBuildLvlReqs = DeserializationHelper.ParseElementToListOfPrimitives<int>(mainBuildLvlReq, '\n');
                if (mainBuildLvlReqs.Count > level)
                {
                    buildingConfig.mainBuildLvlReq = mainBuildLvlReqs[level];
                }
            }

            foodCosts = DeserializationHelper.ParseElementToListOfPrimitives<int>(foodCost, '\n');
            buildingConfig.foodCost = foodCosts[level];

            woodCosts = DeserializationHelper.ParseElementToListOfPrimitives<int>(woodCost, '\n');
            buildingConfig.woodCost = woodCosts[level];

            stoneCosts = DeserializationHelper.ParseElementToListOfPrimitives<int>(stoneCost, '\n');
            buildingConfig.stoneCost = stoneCosts[level];

            durations = DeserializationHelper.ParseElementToListOfPrimitives<string>(duration, '\n');
            buildingConfig.duration = DeserializationHelper.ParseStringIntoSeconds (durations[level]);
            
            if (!string.IsNullOrEmpty(value1))
            {
                value1s = DeserializationHelper.ParseElementToListOfPrimitives<float>(value1, '\n');
                buildingConfig.value1 = value1s[level - 1];
            }

            if (!string.IsNullOrEmpty(value2))
            {
                value2s = DeserializationHelper.ParseElementToListOfPrimitives<float>(value2, '\n');
                buildingConfig.value2 = value2s[level - 1];
            }

            if (!string.IsNullOrEmpty(value3))
            {
                value3s = value3.Split('\n').ToList();
                buildingConfig.value3 = value3s[level - 1];
            }

            ConfigForLevel.Add(level, buildingConfig);
            return buildingConfig;
        }
    }

    public class BuildingConfig
    {
        public string buildingId;
        public string name { get; set; }
        public int level { get; set; }
        public int resource { get; set; }
        public int production { get; set; }
        public int capacity { get; set; }
        public int mainBuildLvlReq { get; set; }
        public int foodCost { get; set; }
        public int woodCost { get; set; }
        public int stoneCost { get; set; }
        public int duration { get; set; }
        public float value1 { get; set; }
        public float value2 { get; set; }
        public string value3 { get; set; }
        public int maxFood { get; set; }
        public int maxWood { get; set; }
        public int maxStone { get; set; }
        public int maxMetal { get; set; }
        public int maxLeather { get; set; }
    }
}