﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using StratosphereGames;
using System.Threading.Tasks;
using StratosphereGames.Base;
using System.Linq;
using Microsoft.AspNetCore.JsonPatch.Internal;

namespace Cloud_Script_Functions
{
    static class GameDataManager
    {
        static Dictionary<string, Dictionary<string, object>> DataDictionariesByName = new Dictionary<string, Dictionary<string, object>>();

        static List<string> loadingTables = new List<string>();

        public static async Task LoadTable<T>() where T : BaseData, new()
        {
            string dataName = typeof(T).Name;
            string strPath = dataName;

            if (DataDictionariesByName.ContainsKey(dataName))
            {
                while (loadingTables.Contains(dataName))
                {
                    await Task.Delay(100);
                    continue;
                }

                return; // todo: cache
                //DataDictionariesByName.Remove(dataName);
            }
            loadingTables.Add(dataName);

            List<T> listOfData = new List<T>();

            // get list
            string str = (await PFCommunication.GetTitleData(new List<string>() { dataName } ,PlayFabDataType.Public, null))[dataName];
            IList listData = Json.Deserialize(str) as IList;

            for (int i = 0; i < listData.Count; ++i)
            {
                var item = listData[i] as Dictionary<string, object>;
                T tmp = GameDataLoader.DictionaryToObject<T>(item);
                listOfData.Add(tmp);
            }
            LoggingHandler.LogFormat(LogType.Log, "Table {0} is loaded with {1} elements.", dataName, listData.Count);

            // turn into dictionary
            Dictionary<string, T> dicForTable;
            bool turnListIntoDicSucc = TurnListIntoDictionary<T>(listOfData, out dicForTable);
            if (!turnListIntoDicSucc)
            {
                LoggingHandler.LogFormat(LogType.Error, "Unable to turn data-list for type '{0}' into dictionary!", dataName);
                return;
            }
            Dictionary<string, object> objDicForTable = new Dictionary<string, object>();
            foreach (var key in dicForTable.Keys)
            {
                objDicForTable.Add(key, (object)dicForTable[key]);
            }
            DataDictionariesByName.Add(dataName, objDicForTable);

            loadingTables.Remove(dataName);
            return;
        }

        private static bool TurnListIntoDictionary<T>(List<T> list, out Dictionary<string, T> dictionary)
            where T : BaseData
        {
            dictionary = new Dictionary<string, T>(StringComparer.OrdinalIgnoreCase);
            foreach (var element in list)
            {
                try
                {
                    dictionary.Add(element.UniqueId, element);
                }
                catch (System.Exception ex)
                {
                    // there can be duplicate
                    LoggingHandler.LogFormat(LogType.Error, "[SLog] TurnListIntoDictioanry of {2} has exceptions - key : {0}, {1}",
                    element.UniqueId,
                    ex.Message,
                    typeof(T).ToString());
                    return false;
                }
            }
            return true;
        }

        public static bool GetDataForTableElement<TData>(string element, out TData result) where TData : BaseData
        {
            string dictionaryName = typeof(TData).Name;
            if (!DataDictionariesByName.ContainsKey(dictionaryName))
            {
                LoggingHandler.Log("didnt find " + dictionaryName, LogType.Error);
                result = default(TData);
                return false;
            }
            if (!DataDictionariesByName[dictionaryName].ContainsKey(element))
            {
                LoggingHandler.Log("didnt find " + element + " in " + dictionaryName, LogType.Error);
                result = default(TData);
                return false;
            }
            result = DataDictionariesByName[dictionaryName][element] as TData;
            return true;
        }

        public static bool GetAllTableData<TData>(out TData[] result) where TData : BaseData
        {
            string dictionaryName = typeof(TData).Name;
            if (!DataDictionariesByName.ContainsKey(dictionaryName))
            {
                LoggingHandler.Log("didnt find " + dictionaryName);
                result = default(TData[]);
                return false;
            }

            var rawDic = DataDictionariesByName[dictionaryName].Values.ToArray();
            var dic = new TData[rawDic.Length];

            for (int i = 0; i < rawDic.Length; i++)
            {
                dic[i] = rawDic[i] as TData;
            }

            result = dic;
            return true;
        }
    } 
}
