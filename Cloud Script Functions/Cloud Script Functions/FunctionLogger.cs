﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cloud_Script_Functions
{
    public class FunctionLogger
    {
        private DateTime initialDate;
        private string functionName;
        private string functionCaller;

        private bool logTime;

        private List<Message> messages;

        public FunctionLogger(string functionName, string functionCaller, bool logTime = true) 
        {
            // store initial date
            initialDate = DateTime.UtcNow;

            this.functionName = functionName;
            this.functionCaller = functionCaller;

            this.logTime = logTime;

            messages = new List<Message>();
        }

        public void AddMessage(string message) 
        {
            if (logTime)
                message = DateTime.UtcNow.ToString("HH:mm:ss") + ": " + message;

            messages.Add(new Message (message));
        }
        
        public void AddMessage(string message, string details)
        {
            if (logTime)
                message = DateTime.UtcNow.ToString("HH:mm:ss") + ": " + message;

            messages.Add(new Message (message, details));
        }

        public void LogEnd() 
        {
            double duration = (DateTime.UtcNow - initialDate).TotalMilliseconds;
            string curDate = "\n[" + DateTime.UtcNow.ToString() + "] ";

            string result = curDate + "Executed function \"" + functionName + "\"\n";
            result += "--> Duration: " + duration.ToString() + " ms \n";
            result += "--> Caller  : " + functionCaller + "\n";
            result += "--> Result  : Function was called successfully";

            LoggingHandler.Log(result, LogType.Log);
        }

        public void LogEnd(string resultMessage)
        {
            double duration = (DateTime.UtcNow - initialDate).TotalMilliseconds;
            string curDate = "\n[" + DateTime.UtcNow.ToString() + "] ";

            string result = curDate + "Executed function \"" + functionName + "\"\n";
            result += "--> Duration: " + duration.ToString() + " ms \n";
            result += "--> Caller  : " + functionCaller + "\n";
            result += "--> Result  : " + resultMessage;

            LoggingHandler.Log(result, LogType.Log);
        }

        public void LogAll(bool logDetails = false) 
        {
            double duration = (DateTime.UtcNow - initialDate).TotalMilliseconds;
            string curDate = "\n[" + DateTime.UtcNow.ToString() + "] ";

            string result = curDate + "Executed \"" + functionName + "\"\n";
            result += "--> Duration: " + duration.ToString() + " ms \n";
            result += "--> Caller  : " + functionCaller + "\n";
            result += "--> Result  : Function was called successfully\n";
            result += "\n";

            foreach (var curMessage in messages)
            {
                result += "--> " + curMessage.header + "\n";
                if (logDetails && curMessage.details != null) result += curMessage.details + "\n";
            }

            LoggingHandler.Log(result, LogType.Log);
        }

        public void LogAll(string resultMessage, bool logDetails = false)
        {
            double duration = (DateTime.UtcNow - initialDate).TotalMilliseconds;
            string curDate = "\n[" + DateTime.UtcNow.ToString() + "] ";

            string result = curDate + "Executed \"" + functionName + "\"\n";
            result += "--> Duration: " + duration.ToString() + " ms \n";
            result += "--> Caller  : " + functionCaller + "\n";
            result += "--> Result  : " + resultMessage + "\n";
            result += "\n";

            foreach (var curMessage in messages)
            {
                result += "--> " + curMessage.header + "\n";
                if (logDetails && curMessage.details != null) result += curMessage.details + "\n";
            }

            LoggingHandler.Log(result, LogType.Log);
        }

        public void LogError(string errorMessage, bool logDetails = true)
        {
            double duration = (DateTime.UtcNow - initialDate).TotalMilliseconds;
            string curDate = "\n[" + DateTime.UtcNow.ToString() + "] ";

            string result = curDate + "Executed \"" + functionName + "\"\n";
            result += "--> Duration: " + duration.ToString() + " ms \n";
            result += "--> Caller  : " + functionCaller + "\n";
            result += "--> Error   : " + errorMessage + "\n";
            result += "\n";

            foreach (var curMessage in messages)
            {
                result += "--> " + curMessage.header + "\n";
                if (logDetails && curMessage.details != null) result += curMessage.details + "\n";
            }


            LoggingHandler.Log(result, LogType.Error);
        }

        public void LogException(string errorMessage, bool logDetails = true) 
        {
            double duration = (DateTime.UtcNow - initialDate).TotalMilliseconds;
            string curDate = "\n[" + DateTime.UtcNow.ToString() + "] ";

            string result = curDate + "Executed \"" + functionName + "\"\n";
            result += "--> Duration  : " + duration.ToString() + " ms \n";
            result += "--> Caller    : " + functionCaller + "\n";
            result += "--> Exception : " + errorMessage + "\n";
            result += "\n";

            foreach (var curMessage in messages)
            {
                result += "--> " + curMessage.header + "\n";
                if (logDetails && curMessage.details != null) result += curMessage.details + "\n";
            }
            
            throw new Exception(result);
        }

        private struct Message 
        {
            public string header;
            public string details;

            public Message(string header)
            {
                this.header = header;
                this.details = null;
            }

            public Message(string header, string details)
            {
                this.header = header;
                this.details = details;
            }
        }
    }
}
