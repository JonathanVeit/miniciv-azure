﻿using System;

using Random = System.Random;

namespace StratosphereGames
{
	
	[Serializable]
	public struct SecuredInt : IConvertible
	{
	    private int value;
	    private int offset;
	
	    public SecuredInt(int v)
	    {
	        offset = new Random().Next(-100, 100);
	        value = offset + v;
	    }
	
	    public SecuredInt(UInt64 v)
	    {
	        offset = new Random().Next(-100, 100);
			value = (int)((UInt64)offset + (UInt64)v);
	    }
	
	    private int Value()
	    {
	        return value - offset;
	    }
	
	    static public implicit operator SecuredInt(int v)
	    {
	        return new SecuredInt(v);
	    }
	
	    static public implicit operator SecuredInt(UInt64 v)
	    {
	        return new SecuredInt(v);
	    }
	
	    static public implicit operator int(SecuredInt si)
	    {
	        return si.Value();
	    }
	
	    static public implicit operator string(SecuredInt si)
	    {
	        return si.Value().ToString();
	    }
	
	    static private SecuredInt ConvertFromObject(UInt64 ui)
	    {
	        SecuredInt si = new SecuredInt(ui);
	        return si;
	    }
	
	    public override string ToString()
	    {
	        return Value().ToString();
	    }
	
	    public TypeCode GetTypeCode()
	    {
	        return this.value.GetTypeCode();
	    }
	
	    public bool ToBoolean(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToBoolean(provider);
	    }
	
	    public byte ToByte(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToByte(provider);
	    }
	
	    public char ToChar(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToChar(provider);
	    }
	
	    public DateTime ToDateTime(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToDateTime(provider);
	    }
	
	    public decimal ToDecimal(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToDecimal(provider);
	    }
	
	    public double ToDouble(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToDouble(provider);
	    }
	
	    public short ToInt16(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToInt16(provider);
	    }
	
	    public int ToInt32(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToInt32(provider);
	    }
	
	    public long ToInt64(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToInt64(provider);
	    }
	
	    public sbyte ToSByte(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToSByte(provider);
	    }
	
	    public float ToSingle(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToSingle(provider);
	    }
	
	    public string ToString(IFormatProvider provider)
	    {
	        return this.value.ToString(provider);
	    }
	
	    public object ToType(Type conversionType, IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToType(conversionType, provider);
	    }
	
	    public ushort ToUInt16(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToUInt16(provider);
	    }
	
	    public uint ToUInt32(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToUInt32(provider);
	    }
	
	    public ulong ToUInt64(IFormatProvider provider)
	    {
	        return ((IConvertible)this.value).ToUInt64(provider);
	    }
	}
}