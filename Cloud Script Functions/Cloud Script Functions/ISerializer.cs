﻿namespace StratosphereGames
{
    public interface ISerializer
    {
        T DeserializeObject<T>(string json) where T : new();
        T DeserializeObject<T>(string json, object jsonSerializerStrategy) where T : new();

        string SerializeObject(object json);
        string SerializeObject(object json, object jsonSerializerStrategy);
    }
}
