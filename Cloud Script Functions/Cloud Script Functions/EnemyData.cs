﻿using StratosphereGames;
using System.Collections.Generic;
using Cloud_Script_Functions;

namespace MiniCiv
{
    public class EnemyData : BaseData
    {
        public string enemyId => UniqueId;
        public string name { get; set; }
        public ArmySetup armySetup { get; set; }
        public Dictionary<string, int> unitLevels { get; set; }
        public CloudScriptFunctions.MatchReward reward { get; set; }
    }
}