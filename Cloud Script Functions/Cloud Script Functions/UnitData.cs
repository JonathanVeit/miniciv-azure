﻿using StratosphereGames;
using System.Collections.Generic;

namespace MiniCiv
{
    public class UnitData : BaseData
    {
        public int epoch { get; set; }
        public string unitId => UniqueId;
        public string name { get; set; }
        public string type { get; set; }
        public string description { get; set; } //TODO: Delete when this is handles by localisation system
        public int health { get; set; }
        public int armor { get; set; }
        public int cost { get; set; }
        public float acceleration { get; set; }
        public float maxSpeed { get; set; }
        public float rotationSpeed { get; set; }
        public string weaponId { get; set; }
        public float size { get; set; }
        public float mass { get; set; }
        public int count { get; set; }
        public string costFood { get; set; }
        public string costWood { get; set; }
        public string costStone { get; set; }
        public string costIron { get; set; }
        public string costLeather { get; set; }
        public string upgradeMaterials { get; set; }
        public string upgradeCostMP { get; set; }
        public bool bActive { get; set; }

        public UnitData CreateCopy()
        {
            UnitData ud = new UnitData();

            ud.UniqueId = UniqueId;
            ud.epoch = epoch;
            ud.name = name;
            ud.type = type;
            ud.description = description;
            ud.health = health;
            ud.armor = armor;
            ud.cost = cost;
            ud.acceleration = acceleration;
            ud.maxSpeed = maxSpeed;
            ud.rotationSpeed = rotationSpeed;
            ud.weaponId = weaponId;
            ud.size = size;
            ud.mass = mass;
            ud.count = count;
            ud.costFood = costFood;
            ud.costWood = costWood;
            ud.costStone = costStone;
            ud.costIron = costIron;
            ud.costLeather = costLeather;
            ud.upgradeMaterials = upgradeMaterials;
            ud.upgradeCostMP = upgradeCostMP;
            ud.bActive = bActive;

            return ud;
        }
    }

    public class ArmySetup
    {
        public bool isActive;
        public List<PlacedUnitData> placedUnits = new List<PlacedUnitData>();
        public List<PlacedHeroData> placedHeroes = new List<PlacedHeroData>();

        public ArmySetup(bool isActive) { this.isActive = isActive; }
    }

    public class PlacedUnitData
    {
        public string uId;
        public float x;
        public float y;
    }

    public class PlacedHeroData : PlacedUnitData
    {
        public string itemId;
    }
}