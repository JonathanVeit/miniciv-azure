﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using PlayFab.ServerModels;
using PlayFab;
using PlayFab.Samples;
using System;

namespace Cloud_Script_Functions
{
    public enum PlayFabDataType {Public, ReadOnly, Internal}

    public static class PFCommunication
    {
        #region Initialize and Title Data
        public static async Task<Dictionary<string, string>> GetTitleData(List<string> keys, PlayFabDataType dataType, FunctionLogger fLogger)
        {
            var request = new GetTitleDataRequest()
            {
                Keys = keys
            };

            switch (dataType)
            {
                case PlayFabDataType.Public:
                    var result = await PlayFabServerAPI.GetTitleDataAsync(request);
                    if (result.Error != null) fLogger.LogException("GetTitleDataAsync error: " + result.Error.ErrorMessage);

                    return result.Result.Data;
                case PlayFabDataType.ReadOnly:
                    fLogger.LogException("There is no read only data in title data!");

                    return default;
                case PlayFabDataType.Internal:
                    result = await PlayFabServerAPI.GetTitleInternalDataAsync(request);
                    if (result.Error != null) fLogger.LogException("GetTitleInternalDataAsync error: " + result.Error.ErrorMessage);

                    return result.Result.Data;
            }

            return default;
        }

        public static async Task<PlayerPlayStreamFunctionExecutionContext<object[]>> GetPlaystreamContext(HttpRequest req)
        {
            string body = await req.ReadAsStringAsync();
            PlayerPlayStreamFunctionExecutionContext<object[]> context = JsonConvert.DeserializeObject<PlayerPlayStreamFunctionExecutionContext<object[]>>(body);
            return context;
        }

        public static async Task<FunctionExecutionContext<dynamic>> GetFunctionExecutionContext(HttpRequest req)
        {
            string body = await req.ReadAsStringAsync();
            FunctionExecutionContext<dynamic> context = JsonConvert.DeserializeObject<FunctionExecutionContext<dynamic>>(body);
            return context;
        }
        #endregion

        #region Character Management
        public static async Task<string> GrantCharacterToPlayer(string playerID, string characterName, string characterType, FunctionLogger fLogger)
        {
            var request = new GrantCharacterToUserRequest
            {
                PlayFabId = playerID,
                CharacterName = characterName,
                CharacterType = characterType,
            };

            var result = await PlayFabServerAPI.GrantCharacterToUserAsync(request);
            if (result.Error != null) fLogger.LogException("GrantCharacterToUserAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Granted character \"" + characterName + "\" to player \"" + playerID + "\"");
            return result.Result.CharacterId;
        }

        public static async Task<string> GetCharacterID(string playerID, string characterName, FunctionLogger fLogger)
        {
            var userData = await GetUserData(playerID, new List<string>() { "Characters" }, PlayFabDataType.Internal, fLogger);

            try
            {
                var charactersDic = JsonConvert.DeserializeObject<Dictionary<string, string>>(userData["Characters"].Value);

                fLogger.AddMessage("(PF) Found and returned character ID for \"" + characterName + "\" (" + charactersDic[characterName] + ") for player \"" + playerID + "\"");
                return charactersDic[characterName];
            }
            catch (Exception e)
            {
                fLogger.LogException("(PF) Cannot deserialize characters from user data to dictionary: " + e.Message);
            }

            return default;
        }

        public static async Task<Dictionary<string, string>> GetCharacterIDs(string playerID, FunctionLogger fLogger)
        {
            var userData = await GetUserData(playerID, new List<string>() { "Characters" }, PlayFabDataType.Internal, fLogger);

            try
            {
                var charactersDic = JsonConvert.DeserializeObject<Dictionary<string, string>>(userData["Characters"].Value);

                fLogger.AddMessage("(PF) Found and returned " + charactersDic.Keys.Count + " character IDs for player \"" + playerID + "\"");
                return charactersDic;
            }
            catch (Exception e)
            {
                fLogger.LogException("(PF) Cannot deserialize characters from user data to dictionary: " + e.Message);
            }

            return default;
        }

        public static async Task<List<ItemInstance>> GetCharacterInventoryByName(string playerID, string characterName, FunctionLogger fLogger)
        {
            var request = new GetCharacterInventoryRequest()
            {
                PlayFabId = playerID,
                CharacterId = await GetCharacterID(playerID, characterName, fLogger),
            };

            var result = await PlayFabServerAPI.GetCharacterInventoryAsync(request);          
            if (result.Error != null) fLogger.LogException("GetCharacterInventoryAsync error: " + result.Error.ErrorMessage);
            
            fLogger.AddMessage("(PF) Found and returned inventory of character \"" + characterName + "\" (" + result.Result.Inventory.Count + " items) for player \"" + playerID + "\"");
            return result.Result.Inventory;
        }

        public static async Task<List<ItemInstance>> GetCharacterInventoryByID(string playerID, string characterID, FunctionLogger fLogger)
        {
            var request = new GetCharacterInventoryRequest()
            {
                PlayFabId = playerID,
                CharacterId = characterID,
            };

            var result = await PlayFabServerAPI.GetCharacterInventoryAsync(request);
            if (result.Error != null) fLogger.LogException("GetCharacterInventoryAsync error: " + result.Error.ErrorMessage);
            
            fLogger.AddMessage("(PF) Found and returned inventory of character \"" + characterID + "\" (" + result.Result.Inventory.Count + " items) for player \"" + playerID + "\"");
            return result.Result.Inventory;
        }
        #endregion

        #region Item Management
        public static async Task MoveItemToCharacter(string playerID, string fromCharacterID, string toCharacterID, string itemInstanceID, FunctionLogger fLogger)
        {
            var request = new MoveItemToCharacterFromCharacterRequest()
            {
                PlayFabId = playerID,
                GivingCharacterId = fromCharacterID,
                ReceivingCharacterId = toCharacterID,
                ItemInstanceId = itemInstanceID
            };

            var result = await PlayFabServerAPI.MoveItemToCharacterFromCharacterAsync(request);
            if (result.Error != null) fLogger.LogException("MoveItemToCharacterFromCharacterAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Moved item \"" + itemInstanceID + "\" from character \"" + fromCharacterID + "\" to character \"" + toCharacterID + "\" of player \"" + playerID + "\"");
        }

        public static async Task UpdateItemCustomData(string playerID, string characterID, string itemInstanceID, Dictionary<string, string> dataToUpdate, FunctionLogger fLogger)
        {
            var request = new UpdateUserInventoryItemDataRequest()
            {
                PlayFabId = playerID,
                CharacterId = characterID,
                ItemInstanceId = itemInstanceID,
                Data = dataToUpdate,
            };

            var result = await PlayFabServerAPI.UpdateUserInventoryItemCustomDataAsync(request);
            if (result.Error != null) fLogger.LogException("UpdateUserInventoryItemCustomDataAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Updated item custom data of item \"" + itemInstanceID + "\" of player \"" + playerID + "\"", JsonConvert.SerializeObject(dataToUpdate, Formatting.Indented));
        }

        public static async Task<GrantItemsToCharacterResult> GrantItemsToCharacter(string playerID, string characterID, string catalogVersion, List<string> itemIDs, FunctionLogger fLogger)
        {
            var request = new GrantItemsToCharacterRequest()
            {
                PlayFabId = playerID,
                CharacterId = characterID,
                CatalogVersion = catalogVersion,
                ItemIds = itemIDs,
            };  

            var result = await PlayFabServerAPI.GrantItemsToCharacterAsync(request);
            if (result.Error != null) fLogger.LogException("GrantItemsToCharacterAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Granted items to character \"" + characterID + "\" of player \"" + playerID + "\"", JsonConvert.SerializeObject(itemIDs, Formatting.Indented));
            return result.Result;
        }

        public static async Task RemoveItemsFromCharacter(string playerID, string characterID, string itemInstanceID, int amount, FunctionLogger fLogger) 
        {
            var request = new ConsumeItemRequest()
            {
                PlayFabId = playerID,
                CharacterId = characterID,
                ItemInstanceId = itemInstanceID,
                ConsumeCount = amount
            };

            var result = await PlayFabServerAPI.ConsumeItemAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) ConsumeItemAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Removed items from character \"" + characterID + "\" of player \"" + playerID + "\"", JsonConvert.SerializeObject(itemInstanceID) + "(" + amount + "x)");
        }

        public static async Task ModifyItemUses(string playerID, string itemInstanceID, int amount, FunctionLogger fLogger)
        {
            var request = new ModifyItemUsesRequest()
            {
                PlayFabId = playerID,
                ItemInstanceId = itemInstanceID,
                UsesToAdd = amount,
            };

            var result = await PlayFabServerAPI.ModifyItemUsesAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) ModifyItemUsesAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Added " + amount + " uses to item " + itemInstanceID + " of player \"" + playerID + "\"");
        }
        #endregion

        #region Player Data Management
        public static async Task<Dictionary<string, int>> GetPlayerCurrencies(string playerID, FunctionLogger fLogger)
        {
            var request = new GetPlayerCombinedInfoRequest()
            {
                PlayFabId = playerID,
                InfoRequestParameters = new GetPlayerCombinedInfoRequestParams()
                {
                    GetUserVirtualCurrency = true,
                }
            };

            var result = await PlayFabServerAPI.GetPlayerCombinedInfoAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) GetPlayerCombinedInfoAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Found and returned currencies of player \"" + playerID + "\"", JsonConvert.SerializeObject(result.Result.InfoResultPayload.UserVirtualCurrency, Formatting.Indented));
            return result.Result.InfoResultPayload.UserVirtualCurrency;
        }

        public static async Task AddCurrencyToPlayer(string playerID, string currencyID, int amount, FunctionLogger fLogger)
        {
            var request = new AddUserVirtualCurrencyRequest()
            {
                PlayFabId = playerID,
                VirtualCurrency = currencyID,
                Amount = amount,
            };

            var result = await PlayFabServerAPI.AddUserVirtualCurrencyAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) AddUserVirtualCurrencyAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Added currency to player \"" + playerID + "\": " + amount + " " + currencyID);
        }

        public static async Task SubstractCurrencyFromPlayer(string playerID, string currencyID, int amount, FunctionLogger fLogger)
        {
            var request = new SubtractUserVirtualCurrencyRequest()
            {
                PlayFabId = playerID,
                VirtualCurrency = currencyID,
                Amount = amount,
            };

            var result = await PlayFabServerAPI.SubtractUserVirtualCurrencyAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) SubtractUserVirtualCurrencyAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Subtracted currency from player \"" + playerID + "\": " + amount + " " + currencyID);
        }

        public static async Task<GetUserAccountInfoResult> GetUserAccountInfo(string playerID, FunctionLogger fLogger)
        {
            var request = new GetUserAccountInfoRequest()
            {
                PlayFabId = playerID, 
            };

            var result = await PlayFabServerAPI.GetUserAccountInfoAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) GetUserAccountInfoAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Found and returned acount info of player \"" + playerID + "\"", JsonConvert.SerializeObject (result.Result, Formatting.Indented));
            return result.Result;
        }

        public static async Task UpdatePlayerStatistics(string playerID, List<StatisticUpdate> statisticsUpdate, FunctionLogger fLogger) 
        {
            var request = new UpdatePlayerStatisticsRequest()
            {
                PlayFabId = playerID,
                Statistics = statisticsUpdate
            };

            var result = await PlayFabServerAPI.UpdatePlayerStatisticsAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) UpdatePlayerStatisticsAsync error: " + result.Error.ErrorMessage);
        
            fLogger.AddMessage("(PF) Updated statistics of player \"" + playerID + "\"", JsonConvert.SerializeObject(statisticsUpdate, Formatting.Indented));
        }

        public static async Task<List<StatisticValue>> GetPlayerStatistics(string playerID, List<string> statisticNames, FunctionLogger fLogger)
        {
            var request = new GetPlayerStatisticsRequest()
            {
                PlayFabId = playerID, 
                StatisticNames = statisticNames,
            };

            var result = await PlayFabServerAPI.GetPlayerStatisticsAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) GetPlayerStatisticsAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Found and returned statistics of player \"" + playerID + "\"", JsonConvert.SerializeObject(result.Result.Statistics, Formatting.Indented));
            return result.Result.Statistics;
        }

        public static async Task<Dictionary<string, UserDataRecord>> GetUserData(string playerID, List<string> keys, PlayFabDataType dataType, FunctionLogger fLogger)
        {
            var request = new GetUserDataRequest()
            {
                PlayFabId = playerID,
                Keys = keys
            };

            switch (dataType)
            {
                case PlayFabDataType.Public:
                    var result = await PlayFabServerAPI.GetUserDataAsync(request);
                    if (result.Error != null) fLogger.LogException("(PF) GetUserDataAsync error: " + result.Error.ErrorMessage);

                    fLogger.AddMessage("(PF) Found and returned user data of player \"" + playerID + "\"", JsonConvert.SerializeObject(result.Result.Data, Formatting.Indented));
                    return result.Result.Data;
                case PlayFabDataType.ReadOnly:
                    result = await PlayFabServerAPI.GetUserReadOnlyDataAsync(request);
                    if (result.Error != null) fLogger.LogException("(PF) GetUserReadOnlyDataAsync error: " + result.Error.ErrorMessage);

                    fLogger.AddMessage("(PF) Found and returned user data of player \"" + playerID + "\"", JsonConvert.SerializeObject(result.Result.Data, Formatting.Indented));
                    return result.Result.Data;
                case PlayFabDataType.Internal:
                    result = await PlayFabServerAPI.GetUserInternalDataAsync(request);
                    if (result.Error != null) fLogger.LogException("(PF) GetUserInternalDataAsync error: " + result.Error.ErrorMessage);

                    fLogger.AddMessage("(PF) Found and returned user data of player \"" + playerID + "\"", JsonConvert.SerializeObject(result.Result.Data, Formatting.Indented));
                    return result.Result.Data;
            }

            return default;
        }

        public static async Task UpdateUserData(string playerID, Dictionary<string, string> dataToUpdate, PlayFabDataType dataType, FunctionLogger fLogger)
        {
            var request = new UpdateUserDataRequest()
            {
                PlayFabId = playerID,
                Data = dataToUpdate,
            };
       
            switch (dataType)
            {
                case PlayFabDataType.Public:
                    var result = await PlayFabServerAPI.UpdateUserDataAsync(request);
                    if (result.Error != null) fLogger.LogException("(PF) UpdateUserDataAsync error: " + result.Error.ErrorMessage);

                    fLogger.AddMessage("(PF) Updated user data of player \"" + playerID + "\"", JsonConvert.SerializeObject(dataToUpdate, Formatting.Indented));
                    break;
                case PlayFabDataType.ReadOnly:
                    result = await PlayFabServerAPI.UpdateUserReadOnlyDataAsync(request);
                    if (result.Error != null) fLogger.LogException("(PF) UpdateUserReadOnlyDataAsync error: " + result.Error.ErrorMessage);

                    fLogger.AddMessage("(PF) Updated user data of player \"" + playerID + "\"", JsonConvert.SerializeObject(dataToUpdate, Formatting.Indented));
                    break;
                case PlayFabDataType.Internal:
                    var request1 = new UpdateUserInternalDataRequest()
                    {
                        PlayFabId = playerID,
                        Data = dataToUpdate,
                    };
                    result = await PlayFabServerAPI.UpdateUserInternalDataAsync(request1);
                    if (result.Error != null) fLogger.LogException("(PF) UpdateUserInternalDataAsync error: " + result.Error.ErrorMessage);

                    fLogger.AddMessage("(PF) Updated user data of player \"" + playerID + "\"", JsonConvert.SerializeObject(dataToUpdate, Formatting.Indented));
                    break;
            }
        }
        #endregion

        #region Match Making
        public static async Task<List<PlayerLeaderboardEntry>> GetLeaderboardAroundPlayer(string playerID, string statisticsName, int maxResultCount, FunctionLogger fLogger)
        {
            var request = new GetLeaderboardAroundUserRequest()
            {
                PlayFabId = playerID,
                MaxResultsCount = maxResultCount,
                StatisticName = statisticsName,
            };

            var result = await PlayFabServerAPI.GetLeaderboardAroundUserAsync(request);
            if (result.Error != null) fLogger.LogException("(PF) GetLeaderboardAroundUserAsync error: " + result.Error.ErrorMessage);

            fLogger.AddMessage("(PF) Found and returned leaderboard around player \"" + playerID + "\"", JsonConvert.SerializeObject(result.Result.Leaderboard, Formatting.Indented));
            return result.Result.Leaderboard;
        }
        #endregion
    }
}

