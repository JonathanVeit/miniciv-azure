﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Cloud_Script_Functions
{
    public class PlayerInitializationData
    {

        public List<PlayerInitializationData> playerData;
        public List<PlayerInitCharacter> characters;

        public string buildingsCharacter;
        public List<string> buildingItemsToAdd;
        public List<PlayerInitBuildingData> buildingItemData;

        public Dictionary<string, int> unitLevels;

        public List<PlayerInitStatistic> statistics;

        public List<string> defaultUnits;
    }

    public class PlayerInitCharacter
    {
        public string characterName;
        public string characterType;
    }

    public class PlayerInitBuildingData 
    {
        public bool useDatetime;
        public string key;
        public string value;
    }

    public class PlayerInitStatistic 
    {
        public string name;
        public int value;
    }
}
