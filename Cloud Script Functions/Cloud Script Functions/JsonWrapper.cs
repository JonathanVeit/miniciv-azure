﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using SimpleJson;

namespace StratosphereGames.Base
{
    public class JsonWrapper
    {
        private static ISerializer instance = new SimpleJsonInstance();
        //private static ISerializer instance = new MiniJsonInstance();

        public static T DeserializeObject<T>(string json) where T : new()
        {
            return instance.DeserializeObject<T>(json);
        }

        public static T DeserializeObject<T>(string json, object jsonSerializeStrategy) where T : new()
        {
            return instance.DeserializeObject<T>(json, jsonSerializeStrategy);
        }

        public static string SerializeObject(object obj)
        {
            return instance.SerializeObject(obj);
        }

        public static string SerializeObject(object obj, object jsonSerializeStrategy)
        {
            return instance.SerializeObject(obj, jsonSerializeStrategy);
        }
    }

    public class SimpleJsonInstance : ISerializer
    {
        public T DeserializeObject<T>(string json) where T : new()
        {
            return SimpleJson.SimpleJson.DeserializeObject<T>(json);
        }

        public T DeserializeObject<T>(string json, object jsonSerializerStrategy) where T : new()
        {
            return SimpleJson.SimpleJson.DeserializeObject<T>(json, (IJsonSerializerStrategy)jsonSerializerStrategy);
        }

        public object DeserializeObject(string json)
        {
            return SimpleJson.SimpleJson.DeserializeObject(json);
        }

        public string SerializeObject(object json)
        {
            return SimpleJson.SimpleJson.SerializeObject(json);
        }

        public string SerializeObject(object json, object jsonSerializerStrategy)
        {
            return SimpleJson.SimpleJson.SerializeObject(json, (IJsonSerializerStrategy)jsonSerializerStrategy);
        }
    }

    public class MiniJsonInstance : ISerializer
    {
        public T DeserializeObject<T>(string json) where T : new()
        {
            object obj = StratosphereGames.Json.Deserialize(json);
            var dict = obj as Dictionary<string, object>;
            T result = GameDataLoader.DictionaryToObject<T>(dict);
            return result;
        }

        public T DeserializeObject<T>(string json, object jsonSerializerStrategy) where T : new()
        {
            return DeserializeObject<T>(json);
        }

        public string SerializeObject(object json)
        {
            Type t = json.GetType();
            bool isGeneric = t.IsGenericType;
            if (isGeneric)
            {
                return Json.Serialize(json);
            }
            var dict = GameDataLoader.ObjectToDictionary(json);
            return Json.Serialize(dict);
        }

        public string SerializeObject(object json, object jsonSerializerStrategy)
        {
            return SerializeObject(json);
        }
    }

    public static class Util
    {
        public static readonly string[] _defaultDateTimeFormats = new string[]{ // All parseable ISO 8601 formats for DateTime.[Try]ParseExact - Lets us deserialize any legacy timestamps in one of these formats
            // These are the standard format with ISO 8601 UTC markers (T/Z)
            "yyyy-MM-ddTHH:mm:ss.FFFFFFZ",
            "yyyy-MM-ddTHH:mm:ss.FFFFZ",
            "yyyy-MM-ddTHH:mm:ss.FFFZ", // DEFAULT_UTC_OUTPUT_INDEX
            "yyyy-MM-ddTHH:mm:ss.FFZ",
            "yyyy-MM-ddTHH:mm:ssZ",

            // These are the standard format without ISO 8601 UTC markers (T/Z)
            "yyyy-MM-dd HH:mm:ss.FFFFFF",
            "yyyy-MM-dd HH:mm:ss.FFFF",
            "yyyy-MM-dd HH:mm:ss.FFF",
            "yyyy-MM-dd HH:mm:ss.FF", // DEFAULT_LOCAL_OUTPUT_INDEX
            "yyyy-MM-dd HH:mm:ss",

            // These are the result of an input bug, which we now have to support as long as the db has entries formatted like this
            "yyyy-MM-dd HH:mm.ss.FFFF",
            "yyyy-MM-dd HH:mm.ss.FFF",
            "yyyy-MM-dd HH:mm.ss.FF",
            "yyyy-MM-dd HH:mm.ss",
        };
        private static DateTimeStyles _dateTimeStyles = DateTimeStyles.RoundtripKind;

        public static MyJsonSerializerStrategy ApiSerializerStrategy = new MyJsonSerializerStrategy();
        public const int DEFAULT_UTC_OUTPUT_INDEX = 2;
        public const int DEFAULT_LOCAL_OUTPUT_INDEX = 8;
        public class MyJsonSerializerStrategy : PocoJsonSerializerStrategy
        {
            /// <summary>
            /// Convert the json value into the destination field/property
            /// </summary>
            public override object DeserializeObject(object value, Type type)
            {
                string valueStr = value as string;
                if (valueStr == null) // For all of our custom conversions, value is a string
                    return base.DeserializeObject(value, type);

                Type underType = Nullable.GetUnderlyingType(type);
                if (underType != null)
                    return DeserializeObject(value, underType);
                else if (type.GetTypeInfo().IsEnum)
                    return Enum.Parse(type, (string)value, true);
                else if (type == typeof(DateTime))
                {
                    DateTime output;
                    bool result = DateTime.TryParseExact(valueStr, _defaultDateTimeFormats, CultureInfo.CurrentCulture, _dateTimeStyles, out output);
                    if (result)
                        return output;
                }
                else if (type == typeof(DateTimeOffset))
                {
                    DateTimeOffset output;
                    bool result = DateTimeOffset.TryParseExact(valueStr, _defaultDateTimeFormats, CultureInfo.CurrentCulture, _dateTimeStyles, out output);
                    if (result)
                        return output;
                }
                else if (type == typeof(TimeSpan))
                {
                    double seconds;
                    if (double.TryParse(valueStr, out seconds))
                        return TimeSpan.FromSeconds(seconds);
                }
                return base.DeserializeObject(value, type);
            }

            /// <summary>
            /// Set output to a string that represents the input object
            /// </summary>
            protected override bool TrySerializeKnownTypes(object input, out object output)
            {
                if (input.GetType().GetTypeInfo().IsEnum)
                {
                    output = input.ToString();
                    return true;
                }
                else if (input is DateTime)
                {
                    output = ((DateTime)input).ToString(_defaultDateTimeFormats[DEFAULT_UTC_OUTPUT_INDEX], CultureInfo.CurrentCulture);
                    return true;
                }
                else if (input is DateTimeOffset)
                {
                    output = ((DateTimeOffset)input).ToString(_defaultDateTimeFormats[DEFAULT_UTC_OUTPUT_INDEX], CultureInfo.CurrentCulture);
                    return true;
                }
                else if (input is TimeSpan)
                {
                    output = ((TimeSpan)input).TotalSeconds;
                    return true;
                }
                return base.TrySerializeKnownTypes(input, out output);
            }
        }
    }
}
