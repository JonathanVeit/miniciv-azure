﻿using Cloud_Script_Functions;
using System;
using System.Collections.Generic;

namespace StratosphereGames.Helpers
{
    public class EnumHelper {
        /// <summary>
        /// Provides faster way for parsing a string to an enum
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="str">Value to be parsed</param>
        /// <returns></returns>
        public static T ParseValue<T>(string str)
        {
            var enumType = typeof(T);

            if (!enumType.IsEnum)
            {
                //DebugHelper.PrintFormatted(UnityEngine.LogType.Warning, "EnumHelper: T has to be an enumerable!");
                return default(T);
            }

            if (string.IsNullOrEmpty(str))
            {
                //DebugHelper.PrintFormatted(UnityEngine.LogType.Warning, "EnumHelper: Given val was null or emtpy!");
                return default(T);
            }

            bool containsName = ContainsElement<T>(str);
            if(containsName)
            {
                T result = (T)Enum.Parse(enumType, str);
                return result;
            }

           // DebugHelper.PrintFormatted(UnityEngine.LogType.Warning, "Enum {0} does not contain element {1}; parsing failed, returning default value", enumType.Name, str);
            return default(T);
        }

        public static T MergeFlags<T>(T val1, T val2)
        {
            int val1AsInt = Convert.ToInt16(val1);
            int val2AsInt = Convert.ToInt16(val2);
            return (T)(object)(val1AsInt | val2AsInt);
        }

        public static bool ContainsElement<T>(string elementName)
        {
            var enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                return false;
            }

            var enumElementNames = Enum.GetNames(enumType);
            foreach (var enumElementName in enumElementNames)
            {
                if (elementName == enumElementName)
                {
                    return true;
                }
            }

            return false;
        }
        /*
        public static T RandomValidElement<T>()
        {
            var enumType = typeof(T);
            if (!enumType.IsEnum)
            {
                return default(T);
            }

            var enumElementNames = Enum.GetNames(enumType);
            List<string> elementList = new List<string>(enumElementNames);

            elementList.Remove("None");
            elementList.Remove("Invalid");
            elementList.Remove("none");
            elementList.Remove("invalid");
            elementList.Remove("NONE");
            elementList.Remove("INVALID");

            string randomElement = elementList.GetRandomElement();

            if(string.IsNullOrEmpty(randomElement))
            {
                return default(T);
            }

            return ParseValue<T>(randomElement);
        }
        */
        public static void ForEachValue<TEnum>(Action<TEnum> act)
        {
            if (!typeof(TEnum).IsEnum)
            {
                LoggingHandler.LogFormat(LogType.Error,
                    "Cannot iterate over type {0}. Must be an Enum!", typeof(TEnum).Name);
                return;
            }
            foreach (TEnum val in Enum.GetValues(typeof(TEnum)))
            {
                act(val);
            }
        }
    }
}
